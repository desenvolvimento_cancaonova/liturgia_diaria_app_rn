import {useEffect} from 'react';
import {AppState, Platform} from 'react-native';
import Tts from 'react-native-tts';

import {LiturgiaInterface} from '~/interfaces/liturgia';
import {OracaoInterface} from '~/interfaces/oracao';
import SantoInterface from '~/interfaces/santo';
import {removeTagEnd, removeTagFirst} from '~/utils/newHtml';
import removeHtml from '~/utils/RemoveHtml';
import useConfigStore from '~/store/config';

interface PlayerProps {
  tab?: number;
  type: 'liturgia' | 'santo' | 'oracao' | 'homilia';
  model: LiturgiaInterface & SantoInterface & OracaoInterface;
}

export enum PlayerState {
  'playing',
  'pause',
  'stop',
}
export interface TPlayer {
  play: ({...rest}: PlayerProps) => void;
  stop: () => void;
  setup: () => void;
}

interface TextPlayerProps {
  isPlayingText: keyof typeof PlayerState;
  setIsPlayingText: (isPlayingText: keyof typeof PlayerState) => void;
}
export const TextPlayer = ({
  isPlayingText,
  setIsPlayingText,
}: TextPlayerProps) => {
  const {playAudioInBackground} = useConfigStore();

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (nextAppState === 'background' && !playAudioInBackground) {
        stop();
      }
    });

    return () => {
      subscription.remove();
    };
  }, [playAudioInBackground]);

  const initPlayer = async () => {
    // MusicControl.enableControl('play', false);
    // MusicControl.enableControl('pause', true);
    // MusicControl.enableControl('stop', true);
    // MusicControl.enableControl('nextTrack', false);
    // MusicControl.enableControl('previousTrack', false);

    Tts.setDucking(true);
    Tts.setDefaultLanguage('pt-BR');
    if (!playAudioInBackground) {
      Tts.setIgnoreSilentSwitch('ignore');
    } else {
      Tts.setIgnoreSilentSwitch('inherit');
    }

    if (Platform.OS === 'ios') {
      Tts.setDefaultRate(0.4);
      // MusicControl.handleAudioInterruptions(true);
    } else {
      Tts.setDefaultRate(0.5);
    }
  };

  useEffect(() => {
    Tts.stop();
    // MusicControl.stopControl();
    initPlayer();
  }, [playAudioInBackground]);

  Tts.addEventListener('tts-cancel', event => {
    stop();
    //console.log('cancel', event);
  });
  // MusicControl.on(Command.pause, () => {
  //   stop();
  // });
  // MusicControl.on(Command.stop, () => {
  //   stop();
  // });

  const stop = () => {
    Tts.stop();
    setIsPlayingText('stop');
    // MusicControl.resetNowPlaying();
  };

  const titleCase = (string: string) => {
    return string[0].toUpperCase() + string.slice(1).toLowerCase();
  };

  const splitText = (text: string, chunkSize = 4000): string[] => {
    if (!text) {
      return [];
    }
    return text.match(new RegExp(`.{1,${chunkSize}}`, 'g')) || [];
  };

  const speakChunks = async (chunks: string[]) => {
    for (index = 0; index < chunks.length; index++) {
      Tts.speak(chunks[index]);
    }
  };

  const play = async ({tab, type, model}: PlayerProps) => {
    Tts.stop();

    // MusicControl.setNowPlaying({
    //   title: model.titulo,
    //   artwork: require('~/assets/images/icon.png'),
    //   artist: titleCase(type),
    // });

    if (type === 'liturgia') {
      if (isPlayingText === 'stop') {
        setIsPlayingText('playing');
        switch (tab) {
          case 0:
            Tts.speak(removeHtml(model.leitura1));
            break;
          case 1:
            Tts.speak(removeHtml(model.leitura2));
            break;
          case 2:
            Tts.speak(removeHtml(model.salmo));
            break;
          case 3:
            Tts.speak(removeHtml(model.evangelho));
            break;
          default:
            break;
        }
        return;
      }
      stop();
      return;
    }
    if (type === 'oracao') {
      if (isPlayingText === 'stop') {
        setIsPlayingText('playing');
        // const text = removeHtml(removeTagBody(model.texto));

        const regex = /<body.*?>([\s\S]*)<\/body>/gm;
        const body = regex.exec(`${model.texto}`);
        const text = body[0].replace(/<[^>]+>/g, '').replace('&nbsp;', '');

        Tts.speak(text.substring(0, 3500).trim());

        return;
      }
      stop();
      return;
    }
    if (type === 'santo') {
      if (isPlayingText === 'stop') {
        setIsPlayingText('playing');

        const text = removeHtml(model.titulo + removeTagEnd(model.texto));

        if (text.length > 4000) {
          const chunks = splitText(text);
          await speakChunks(chunks);
        } else {
          Tts.speak(text);
        }
        return;
      }
      stop();
      return;
    }
  };

  const setup = () => {
    Tts.setDefaultLanguage('pt-BR');
  };

  return {
    play,
    stop,
    setup,
  } as TPlayer;
};
