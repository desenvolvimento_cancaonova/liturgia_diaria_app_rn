import analytics from '@react-native-firebase/analytics';
export interface IEventParams {
  id: string;
  item: string;
  description: string;
}
export interface IScreenParams {
  screen_class: string;
}
const addEvent = async (name: string, params?: IEventParams) => {
  await analytics().logEvent(name, params);
};
const logScreen = async (params: IScreenParams) => {
  await analytics().logScreenView(params);
};
export {addEvent, logScreen};
