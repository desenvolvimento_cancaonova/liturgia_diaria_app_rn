import {useEffect, useState} from 'react';
import {AppState, Platform} from 'react-native';
import MusicControl from 'react-native-music-control';
import SoundPlayer from 'react-native-sound-player';
import TrackPlayer, {
  Capability,
  Event,
  useTrackPlayerEvents,
  useProgress,
} from 'react-native-track-player';
import {PlayerState} from '../textPlayer';
import useConfigStore from '~/store/config';

const PlaybackService = async function () {
  TrackPlayer.addEventListener(Event.RemotePlay, () => {
    TrackPlayer.play();
    // setIsPlaying('playing');
  });

  TrackPlayer.addEventListener(Event.RemotePause, () => {
    TrackPlayer.pause();
    // setIsPlaying('stop');
  });
};

// TrackPlayer.registerPlaybackService(() => PlaybackService);

export interface APlayer {
  toggle: (audioUrl: string, title: string, forceStop?: boolean) => void;
  prev: () => void;
  rev: () => void;
  isSeeking: boolean;
  setIsSeeking: (isSeeking: boolean) => void;
  seek: number;
  setSeek: (seek: number) => void;
  duration: number;
  position: number;
  dismountPlayer: () => void;
}

interface AudioPlayerProps {
  isPlaying: keyof typeof PlayerState;
  setIsPlaying: (isPlaying: keyof typeof PlayerState) => void;
}

export const AudioPlayer = ({isPlaying, setIsPlaying}: AudioPlayerProps) => {
  const {playAudioInBackground} = useConfigStore();

  const progress = useProgress(250);
  const {duration, position} = progress;
  const [isSeeking, setIsSeeking] = useState(false);
  const [seek, setSeek] = useState(0);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (['background', 'inactive'].includes(nextAppState)) {
        
        if (Platform.OS === 'ios' && !playAudioInBackground) {
          setIsPlaying('pause');
          TrackPlayer.pause();
        } else {
          if (!playAudioInBackground) {
            SoundPlayer.pause();
            setIsPlaying('pause');
            MusicControl.updatePlayback({
              state: MusicControl.STATE_PAUSED,
            });
          }
        }
      }
    });

    return () => {
      subscription.remove();
    };
  }, [playAudioInBackground]);

  async function setup() {
    if (Platform.OS === 'ios') {
      await TrackPlayer.setupPlayer({});
      await TrackPlayer.updateOptions({
        capabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.JumpBackward,
          Capability.JumpForward,
        ],
        compactCapabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.JumpBackward,
          Capability.JumpForward,
        ],
        notificationCapabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.JumpBackward,
          Capability.JumpForward,
        ],
      });
      return;
    }

    MusicControl.enableBackgroundMode(true);
    // MusicControl.enableControl('closeNotification', true, {when: 'always'});
    MusicControl.enableControl('play', true);
    MusicControl.enableControl('pause', true);
    MusicControl.enableControl('stop', true);
    MusicControl.enableControl('nextTrack', false);
    MusicControl.enableControl('previousTrack', false);
    MusicControl.enableControl('skipForward', true);
    MusicControl.enableControl('skipBackward', true);
    MusicControl.enableControl('seek', false);
    MusicControl.enableControl('changePlaybackPosition', false);
  }

  const dismountPlayer = async () => {
    const unsubscribe = () => {};
    unsubscribe();
    setIsPlaying('stop');
    if (Platform.OS === 'ios') {
      await TrackPlayer.reset();
    } else {
      SoundPlayer.stop();
    }
  };

  const toggleSoundPlayer = async (
    audioUrl: string,
    title: string,
    forceStop: boolean = false,
  ) => {
    try {
      if (forceStop) {
        MusicControl.resetNowPlaying();
        SoundPlayer.pause();
        setIsPlaying('pause');
        MusicControl.updatePlayback({
          state: MusicControl.STATE_PAUSED,
        });
        // return;
      }

      // or play from url
      if (isPlaying === 'stop' && !forceStop) {
        MusicControl.setNowPlaying({
          title,
          artwork: require('~/assets/images/icon.png'),
          artist: 'Canção Nova',
          album: 'Homilia Diaria',
        });
        SoundPlayer.playUrl(audioUrl);
        setIsPlaying('playing');
        MusicControl.updatePlayback({
          state: MusicControl.STATE_PLAYING,
        });
      } else if (isPlaying === 'playing' && !forceStop) {
        SoundPlayer.pause();
        setIsPlaying('pause');
        MusicControl.updatePlayback({
          state: MusicControl.STATE_PAUSED,
        });
      } else if (isPlaying === 'pause' && !forceStop) {
        SoundPlayer.resume();
        setIsPlaying('playing');
        MusicControl.updatePlayback({
          state: MusicControl.STATE_PLAYING,
        });
      }
    } catch (e) {
      console.log('cannot play the sound file', e);
    }
  };

  const togglePlayback = async (
    audioUrl: string,
    title: string,
    forceStop: boolean = false,
  ) => {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack === undefined || currentTrack === null) {
      await TrackPlayer.reset();
      await TrackPlayer.add({
        id: '01',
        url: audioUrl,
        title,
        artist: 'Canção Nova',
        artwork: require('~/assets/images/icon.png'),
        duration,
      });

      setIsPlaying('playing');
      await TrackPlayer.play();
    } else if (isPlaying === 'pause' && !forceStop) {
      setIsPlaying('playing');
      await TrackPlayer.play();
    } else if (isPlaying === 'playing' || forceStop) {
      setIsPlaying('pause');
      await TrackPlayer.pause();
    }
  };

  const toggle = (audioUrl: string, title: string, forceStop = false) => {
    if (Platform.OS === 'ios') {
      togglePlayback(audioUrl, title, forceStop);
    } else {
      toggleSoundPlayer(audioUrl, title, forceStop);
    }
  };

  const prev = async () => {
    if (Platform.OS === 'ios') {
      TrackPlayer.seekTo(position + 10);
    } else {
      const {currentTime} = await SoundPlayer.getInfo();
      SoundPlayer.seek(currentTime + 10);
    }
  };

  const rev = async () => {
    if (Platform.OS === 'ios') {
      TrackPlayer.seekTo(position - 10);
    } else {
      const {currentTime} = await SoundPlayer.getInfo();
      SoundPlayer.seek(currentTime - 10);
    }
  };

  useTrackPlayerEvents(
    [
      Event.RemoteJumpBackward,
      Event.RemoteJumpForward,
      Event.RemotePlay,
      Event.RemotePause,
    ],
    async event => {
      if (event.type === 'remote-jump-forward') {
        prev();
        return;
      }
      if (event.type === 'remote-jump-backward') {
        rev();
        return;
      }
      if (event.type === 'remote-play') {
        setIsPlaying('playing');
        await TrackPlayer.play();
        return;
      }
      if (event.type === 'remote-pause') {
        setIsPlaying('pause');
        await TrackPlayer.pause();
      }
    },
  );

  useEffect(() => {
    setSeek(0);
    setIsSeeking(false);
    setup();
    // setupControl();
  }, [playAudioInBackground]);

  useEffect(() => {
    if (isPlaying === 'playing') {
      setIsSeeking(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [position]);

  return {
    toggle,
    prev,
    rev,
    isSeeking,
    seek,
    duration,
    dismountPlayer,
    setIsSeeking,
    setSeek,
    position,
  } as APlayer;
};
