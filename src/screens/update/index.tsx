import AsyncStorage from '@react-native-async-storage/async-storage';
import {CommonActions} from '@react-navigation/native';
import moment from 'moment';
import React, {useCallback, useEffect, useState} from 'react';
import Preload from '~/components/preload';
import {useRequest} from '~/context/requestContext';
import {useTheme} from '~/context/themeContext';
import {NavigationProps} from '~/interfaces/global';

import Homilia from '../../repository/database/model/homilia';
import Liturgia from '../../repository/database/model/liturgia';
import Oracao from '../../repository/database/model/oracao';
import Santo from '../../repository/database/model/santo';
import crashlytics from '@react-native-firebase/crashlytics';

const Update = ({navigation}: NavigationProps) => {
  const {setTextZoom} = useTheme();
  const [info, setInfo] = useState('dados');
  const {getHomilia, getLiturgia, getOracao, getSanto, getNotifications} =
    useRequest();
  let date: string | Promise<string> = null;
  let week: moment.MomentInput = null;

  const getDate = useCallback(async () => {
    const dataDate = await AsyncStorage.getItem('@date');
    if (dataDate !== null) {
      return (date = dataDate);
    }

    return (date = '');
  }, []);

  const getWeek = useCallback(async () => {
    const weekDate = await AsyncStorage.getItem('@week');
    if (weekDate !== null) {
      return (week = weekDate);
    }
    return (date = '');
  }, []);

  const getZoom = useCallback(async () => {
    const zoomData = await AsyncStorage.getItem('@zoom');
    if (zoomData !== null) {
      setTextZoom(Number(zoomData));
    }
  }, [setTextZoom]);

  const Updates = useCallback(async () => {
    try {
      const now = new Date().getTime();
      const limit = 7200000; // 2h => 7200000; 2min => 120000
      const limitSantos = 43200000; // 12h => 43200000; 2min => 120000
      const lastUpdateTime = await AsyncStorage.getItem('lastUpdateTime');
      const lastUpdateSantosTime = await AsyncStorage.getItem(
        'lastUpdateSantosTime',
      );
      const diffSantos = now - Number(lastUpdateSantosTime);
      const diff = now - Number(lastUpdateTime);

      if (!lastUpdateTime || diff >= limit) {
        await getDate();
        await getWeek();
        AsyncStorage.setItem('lastUpdateTime', String(new Date().getTime()));
        const oracaoObj = new Oracao();
        const liturgiaObj = new Liturgia();
        const homiliaObj = new Homilia();

        const oracao = oracaoObj.all();

        setInfo('Liturgia');
        await getLiturgia();
        setInfo('Homilia');
        await getHomilia();
        await getNotifications();

        if (oracao.length === 0) {
          setInfo('Orações');
          await getOracao();
        } else {
          if (
            !week ||
            moment(String(week).split('/').reverse().join('-'))
              .add(7, 'd')
              .format('L') === moment().format('L')
          ) {
            setInfo('Orações');
            await getOracao();
          }
        }

        await homiliaObj.clearDays(10);
        await liturgiaObj.clearDays(10);
      }

      if (!lastUpdateSantosTime || diffSantos >= limitSantos) {
        AsyncStorage.setItem(
          'lastUpdateSantosTime',
          String(new Date().getTime()),
        );
        const santoObj = new Santo();
        const santo = santoObj.all();

        if (santo.length === 0) {
          setInfo('Santos');
          await getSanto();
        } else {
          if (
            !week ||
            moment(String(week).split('/').reverse().join('-'))
              .add(7, 'd')
              .format('L') === moment().format('L')
          ) {
            setInfo('Santos');
            await getSanto();
          }
        }
      }

      await getZoom();

      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Main'}],
        }),
      );
    } catch (error: any) {
      crashlytics().recordError(
        new Error(`updates method error: ${error.message}`),
      );
    }
  }, [
    getDate,
    getHomilia,
    getLiturgia,
    getNotifications,
    getOracao,
    getSanto,
    getWeek,
    getZoom,
    navigation,
    week,
  ]);

  useEffect(() => {
    Updates();
  }, [Updates]);

  return <Preload info={info} />;
};

export default Update;
