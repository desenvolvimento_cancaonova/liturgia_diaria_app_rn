import {useFocusEffect} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Controls from '~/components/controls';
import Top from '~/components/top/index';
import WatchScreen from '~/components/videoPlayer/videoPlayer';
import {WebView} from '~/components/webview';
import {useDate} from '~/context/dateContext';
import {useRequest} from '~/context/requestContext';
import {useTheme} from '~/context/themeContext';
import {NavigationProps} from '~/interfaces/global';
import {screenView} from '~/services/firebase';
import {bodyColorLight} from '~/styles/theme';
import Header from '../../components/header';
import HomiliaModel from '../../repository/database/model/homilia';
import moment from 'moment';
import normalize from '../../utils/DynamicRatio';
import fonts from '../../styles/fonts';
import crashlytics from '@react-native-firebase/crashlytics';

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const Homilia = ({navigation}: NavigationProps) => {
  const webviewRef: any = useRef(null);

  const {theme} = useTheme();
  const {homiliaModel, setDates, setPage} = useDate();
  const [pause, setPause] = useState(false);
  const {getHomilia} = useRequest();
  const [audioStartEvent, setAudioStartEvent] = useState(null);
  const [videoStartEvent, setVideoStartEvent] = useState(null);
  const [loading, setLoading] = useState(false);

  const getDays = () => {
    try {
      const dates: any[] = [];
      const homilia = new HomiliaModel().getAll();
      for (let i = 0; i < homilia.length; i++) {
        if (moment(homilia[i].dia) <= moment()) {
          dates.push(homilia[i].dia);
        }
      }

      setDates(dates);
    } catch (error: any) {
      crashlytics().recordError(new Error(`getDays error: ${error.message}`));
    } finally {
      setLoading(false);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      getDays();
      setPage('Homilia');
      screenView('Homilia');
    }, []),
  );

  const retryHomilia = async () => {
    setLoading(true);
    try {
      await getHomilia();
    } catch (error: any) {
      crashlytics().recordError(
        new Error(`retry Homilia error: ${error.message}`),
      );
    } finally {
      setTimeout(getDays, 2500);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      const unsubscribe = () => setPause(true);
      unsubscribe();
      return () => {
        setPause(false);
      };
    }, [navigation]),
  );

  return (
    <SafeAreaView style={[{flex: 1}, bodyColorLight(theme)]}>
      <Header title="Homilia" navigation={navigation} />
      <ScrollView overScrollMode="never" style={bodyColorLight(theme)}>
        {homiliaModel && (
          <Top
            dia_formatado={homiliaModel.dia_formatado}
            mes={homiliaModel.mes}
            ano={homiliaModel.ano}
          />
        )}
        <View>
          <View style={{alignItems: 'center'}}>
            {homiliaModel && (
              <>
                <Controls
                  url={homiliaModel?.url}
                  handleCopy={() =>
                    new HomiliaModel().textoToCopy(homiliaModel)
                  }
                  title={'Homilia'}
                  type="homilia"
                  zoomIn={() => {
                    if (webviewRef.current) {
                      webviewRef.current.zoomIn();
                    }
                  }}
                  zoomOut={() => {
                    if (webviewRef.current) {
                      webviewRef.current.zoomOut();
                    }
                  }}
                  audioUrl={homiliaModel.audio}
                />

                <WatchScreen
                  audioStartEvent={audioStartEvent}
                  setVideoStartEvent={setVideoStartEvent}
                  videoUrl={homiliaModel.video}
                />
              </>
            )}
          </View>
        </View>
        {homiliaModel && pause ? (
          <View style={{paddingHorizontal: 17}}>
            <WebView
              ref={webviewRef}
              content={new HomiliaModel().formatedText(homiliaModel.texto)}
            />
          </View>
        ) : (
          <View style={{alignItems: 'center'}}>
            <Text
              style={{marginTop: 20, alignSelf: 'center', color: '#333333'}}>
              Conteúdo não disponível ou ainda não baixado.
            </Text>
            <TouchableOpacity
              onPress={retryHomilia}
              style={{
                borderRadius: normalize(20),
                paddingVertical: normalize(10),
                paddingHorizontal: normalize(14),
                marginTop: normalize(20),
                marginBottom: normalize(20),
                backgroundColor: '#194976',
              }}>
              <Text
                style={{
                  fontSize: normalize(14),
                  color: '#FFF',
                  fontFamily: fonts.REGULAR,
                }}>
                Tentar baixar conteúdo agora
              </Text>
            </TouchableOpacity>
            {loading && <ActivityIndicator size={'small'} color={'#AAAAAA'} />}
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Homilia;
