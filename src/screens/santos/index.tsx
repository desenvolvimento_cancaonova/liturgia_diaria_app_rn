import {useFocusEffect} from '@react-navigation/native';
import moment from 'moment';
import React, {useEffect, useRef, useState} from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';
import Controls from '~/components/controls';
import Top from '~/components/top/index';
import {WebView} from '~/components/webview';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {NavigationProps} from '~/interfaces/global';
import Santo from '~/repository/database/model/santo';
import {screenView} from '~/services/firebase';
import {bodyColorLight} from '~/styles/theme';
import {removeTagEnd, removeTagFirst} from '~/utils/newHtml';
import Header from '../../components/header';

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const Santos = ({navigation}: NavigationProps) => {
  const webviewRef: any = useRef(null);
  const {santoModel, setDates, setPage} = useDate();
  const [offset, setOffset] = useState(0);
  const [showColabore, setShowColabore] = useState(true);
  const {theme} = useTheme();

  const getDays = () => {
    const currentTime = new Date();
    const year = currentTime.getFullYear();
    const dates: any[] = [];
    const santos = new Santo().getAll();
    santos.forEach(item =>
      dates.push(moment(year + '-' + item.dia.split('/').reverse().join('-'))),
    );

    setDates(dates);
  };

  useEffect(() => {
    getDays();
    screenView('Santos');
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      setPage('Santos');
    }, []),
  );

  const anoAtual = new Date().getFullYear();

  return (
    <SafeAreaView style={[{flex: 1}, bodyColorLight(theme)]}>
      <Header
        title="Santo"
        navigation={navigation}
        onSelectDay={undefined}
        hasGoBack={undefined}
      />
      <ScrollView style={bodyColorLight(theme)}>
        {santoModel && (
          <Top
            dia_formatado={santoModel.dia_formatado}
            mes={santoModel.mes}
            ano={anoAtual.toString()}
            title={santoModel.titulo}
          />
        )}
        <Controls
          type="santo"
          model={santoModel}
          url={santoModel?.url}
          handleCopy={() => new Santo().textoToCopy(santoModel)}
          title={'Santo'}
          zoomIn={() => {
            if (webviewRef.current) {
              webviewRef.current.zoomIn();
            }
          }}
          zoomOut={() => {
            if (webviewRef.current) {
              webviewRef.current.zoomOut();
            }
          }}
        />
        {/* <Player type="santo" model={santoModel} /> */}
        <View style={[{paddingHorizontal: 17, flex: 1}, bodyColorLight(theme)]}>
          {santoModel && (
            <WebView
              ref={webviewRef}
              style={bodyColorLight(theme)}
              content={
                removeTagFirst(santoModel.cabecalho) +
                removeTagEnd(santoModel.texto) +
                '<p>&nbsp;</p>'
              }
            />
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Santos;
