import {useFocusEffect} from '@react-navigation/native';
import React from 'react';
import {useDate} from '~/context/dateContext';
import Routes from './routes';

const Sobre = () => {
  const {setPage} = useDate();
  useFocusEffect(() => {
    setPage('Sobre_enviar_sugestoes');
  });
  return <Routes />;
};

export default Sobre;
