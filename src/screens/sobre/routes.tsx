import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';
import Configs from './screens/configs';
import Home from './screens/home';
import Suggestions from './screens/suggestions';
import Terms from './screens/terms';
import Notifications from './screens/notifications';

const Stack = createNativeStackNavigator();

export default function Routes() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Configs" component={Configs} />
      <Stack.Screen name="Suggestions" component={Suggestions} />
      <Stack.Screen name="Terms" component={Terms} />
      <Stack.Screen name="Notifications" component={Notifications} />
    </Stack.Navigator>
  );
}
