import React, {useEffect} from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import Header from '~/components/header';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {screenView} from '~/services/firebase';
import {bodyColorLight, textColorLight} from '~/styles/theme';
import {styles} from './styles';

const Terms = () => {
  const {theme} = useTheme();
  const {setPage} = useDate();

  useEffect(() => {
    screenView('Termos');
    setPage('Sobre_Termos');
  }, [setPage]);

  return (
    <SafeAreaView style={styles.container}>
      <Header
        title="Termos"
        onSelectDay={undefined}
        navigation={undefined}
        hasGoBack={true}
      />
      <View style={[bodyColorLight(theme), styles.main]}>
        <ScrollView>
          <View>
            <Text style={[styles.title, textColorLight(theme)]}>
              Política de Privacidade
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              A Canção Nova leva a política de proteção de sua privacidade muito
              a sério e cumpre todas as legislações aplicáveis relacionadas à
              privacidade e proteção de dados pessoais. Esta Política de
              Privacidade demonstra como lidamos com suas informações pessoais.
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              Ao utilizar o aplicativo Liturgia Canção Nova automaticamente você
              concorda com os termos aqui descritos.
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              Coleta de Informações Para que a experiência de uso do aplicativo
              seja personalizada, o aplicativo recolhe algumas informações sobre
              o modelo do aparelho e o sistema operacional utilizando a
              ferramenta Google Analytics. Estas informações também são usadas
              para monitorar o desempenho e desenvolver melhorias para o
              aplicativo.
            </Text>
            <Text style={[styles.title, textColorLight(theme)]}>
              Google Analytics
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              Este aplicativo usa o Google Analytics, um serviço análise
              fornecido pelo Google, Inc. (“Google”). A informação coletadas
              pelo aplicativo serão transmitidas e armazenadas pelo Google em
              servidores nos Estados Unidos.Google pode igualmente transferir
              esta informação para terceiros, sempre que exigido por lei, ou
              caso tais terceiros processem a informação em nome do Google.
              Google não associará o seu endereço IP com quaisquer outros dados
              mantidos pelo Google. Ao utilizar este aplicativo você concorda
              com o processamento de dados pelo Google na forma e para os
              efeitos descritos acima.
            </Text>
            <Text style={[styles.title, textColorLight(theme)]}>
              Notificações agendadas
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              A Canção Nova estima o respeito e a liberdade dos usuários de seus
              aplicativos, sendo assim somente enviaremos notificações mediante
              ao agendamento de notificações feita através do nosso aplicativo.
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              Push Notification O push notification é uma funcionalidade que
              permite que a Canção Nova envie mensagens aos usuários do
              aplicativo Liturgia Canção Nova a fim de notificá-los sobre um
              determinado assunto sem o agendamento prévio. A Canção Nova se
              compromete em enviar apenas caso o usuário autorize essa opção
              durante o processo de instalação.
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              Alterações na Política de Privacidade A Canção Nova pode adicionar
              ou alterar os termos de política de privacidade de tempos em
              tempos e se compromete em notificar seus usuários através de uma
              nota oficial ou em uma nota de atualização.
            </Text>
            <Text style={[styles.title, textColorLight(theme)]}>
              Como nos contatar
            </Text>
            <Text style={[styles.description, textColorLight(theme)]}>
              Se você tiver quaisquer dúvidas, por favor, faça contato conosco
              por meio do e-mail : desenvolvimento@cancaonova.com
            </Text>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Terms;
