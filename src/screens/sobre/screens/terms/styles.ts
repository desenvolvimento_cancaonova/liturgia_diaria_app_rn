import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  main: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 10,
    fontFamily: fonts.BOLDER,
    paddingLeft: 17,
    paddingRight: 17,
  },
  description: {
    fontSize: 18,
    marginBottom: 10,
    fontFamily: fonts.REGULAR,
    paddingLeft: 17,
    paddingRight: 17,
  },
});
