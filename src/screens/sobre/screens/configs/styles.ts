import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';

const styles = StyleSheet.create({
  aboutContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  configContainer: {
    justifyContent: 'center',
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
  },
  main: {
    flex: 1,
  },
  button: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
  textButton: {
    fontFamily: fonts.BOLD,
    fontSize: 16,
    paddingBottom: 19,
    marginTop: 16,
    color: 'black',
  },
  text: {
    fontFamily: fonts.REGULAR,
    fontSize: 16,
    marginTop: 16,
    lineHeight: 24,
    color: 'black',
  },
  picker: {
    borderColor: '#E5E5E5',
    borderWidth: 1,
    marginTop: 8,
    marginBottom: 242,
  },
});

export default styles;
