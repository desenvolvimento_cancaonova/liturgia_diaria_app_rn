import {Picker} from '@react-native-picker/picker';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  Linking,
  Platform,
  Switch,
  Text,
  View,
  AppState,
} from 'react-native';
import Dropdown from '~/components/dropdown';
import Header from '~/components/header';
import Logo from '~/components/logo';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {screenView} from '~/services/firebase';
import {bodyColorLight, textColorLight} from '~/styles/theme';
import styles from './styles';
import useConfigStore from '~/store/config';
import {useIsFocused} from '@react-navigation/native';
import PushNotification from 'react-native-push-notification';

const Configs = () => {
  const [selectedLanguage, setSelectedLanguage] = useState({});
  const {theme} = useTheme();
  const {
    isEnabled,
    receivePushNotification,
    setPage,
    pushInAppEnabled,
    receivePushInAppNotification,
  } = useDate();
  const {playAudioInBackground, setPlayAudioInBackground} = useConfigStore();
  const isFocused = useIsFocused();
  const [appState, setAppState] = useState(AppState.currentState);

  const openSettings = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL('app-settings:');
    } else {
      Linking.openSettings();
    }
  };

  const alternarSwitch = () => {
    PushNotification.checkPermissions(permissions => {
      if (permissions.alert) {
        Alert.alert(
          'Notificações Ativadas',
          'Por favor, desabilite as notificações nas configurações do dispositivo.',
          [
            {text: 'Cancelar', style: 'cancel'},
            {
              text: 'Abrir Configurações',
              onPress: openSettings,
            },
          ],
        );
      } else {
        Alert.alert(
          'Notificações Desativadas',
          'Por favor, habilite as notificações nas configurações do dispositivo.',
          [
            {text: 'Cancelar', style: 'cancel'},
            {
              text: 'Abrir Configurações',
              onPress: openSettings,
            },
          ],
        );
      }
    });
  };

  const changePushInAppEnabled = () => {
    receivePushInAppNotification(!pushInAppEnabled);
  };

  const changeIsPlayInBG = () => {
    setPlayAudioInBackground(!playAudioInBackground);
  };
  const options = [{label: '10 dias', value: 1}];

  useEffect(() => {
    screenView('Configurações');
    setPage('sobre_configuracoes');
  }, [setPage]);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (appState.match(/inactive|background/) && nextAppState === 'active') {
        PushNotification.checkPermissions(permissions => {
          if (permissions.alert) {
            receivePushNotification(true);
          } else {
            receivePushNotification(false);
          }
        });
      }
      setAppState(nextAppState);
    });

    return () => {
      subscription.remove();
    };
  }, [appState, receivePushNotification]);

  useEffect(() => {
    if (isFocused) {
      PushNotification.checkPermissions(permissions => {
        if (permissions.alert) {
          receivePushNotification(true);
        } else {
          receivePushNotification(false);
        }
      });
    }
  }, [isFocused, receivePushNotification]);

  return (
    <>
      <View style={[styles.aboutContainer, bodyColorLight(theme)]}>
        <Header
          title="Configurações"
          onSelectDay={undefined}
          navigation={undefined}
          hasGoBack={true}
        />
        <Logo />
        <View style={[styles.configContainer, styles.main]}>
          <View style={styles.button}>
            <Text style={[styles.textButton, textColorLight(theme)]}>
              Receber Notificações
            </Text>
            <Switch
              value={isEnabled}
              trackColor={{
                false: theme === 'Claro' ? '#C8C8C8' : '#C8C8C8',
                true: theme === 'Claro' ? '#19497640' : '#C1C1C1',
              }}
              thumbColor={
                !isEnabled
                  ? theme === 'Claro'
                    ? '#7f8c8d'
                    : '#C1C1C1'
                  : theme === 'Claro'
                  ? '#194976'
                  : '#FFFFFF'
              }
              onValueChange={alternarSwitch}
            />
          </View>
          <View style={styles.button}>
            <Text style={[styles.textButton, textColorLight(theme)]}>
              Mensagens dentro do aplicativo
            </Text>
            <Switch
              value={pushInAppEnabled}
              trackColor={{
                false: theme === 'Claro' ? '#C8C8C8' : '#C8C8C8',
                true: theme === 'Claro' ? '#19497640' : '#C1C1C1',
              }}
              thumbColor={
                !pushInAppEnabled
                  ? theme === 'Claro'
                    ? '#7f8c8d'
                    : '#C1C1C1'
                  : theme === 'Claro'
                  ? '#194976'
                  : '#FFFFFF'
              }
              onValueChange={changePushInAppEnabled}
            />
          </View>
          <View style={styles.button}>
            <Text style={[styles.textButton, textColorLight(theme)]}>
              Tocar áudio em segundo plano
            </Text>
            <Switch
              value={playAudioInBackground}
              trackColor={{
                false: theme === 'Claro' ? '#C8C8C8' : '#C8C8C8',
                true: theme === 'Claro' ? '#19497640' : '#C1C1C1',
              }}
              thumbColor={
                !playAudioInBackground
                  ? theme === 'Claro'
                    ? '#7f8c8d'
                    : '#C1C1C1'
                  : theme === 'Claro'
                  ? '#194976'
                  : '#FFFFFF'
              }
              onValueChange={changeIsPlayInBG}
            />
          </View>
          <Text style={[styles.text, textColorLight(theme)]}>
            Período de dias de armazenamento das leituras e homilias.
          </Text>
          {Platform.OS === 'android' ? (
            <View style={styles.picker}>
              <Picker
                dropdownIconColor={theme === 'Claro' ? '#253138' : '#FFFFFF'}
                style={textColorLight(theme)}
                selectedValue={selectedLanguage.value}
                onValueChange={itemValue => setSelectedLanguage(itemValue)}>
                <Picker.Item label="10 dias" value="1" />
              </Picker>
            </View>
          ) : (
            <>
              <View style={{marginBottom: 50}} />
              <Dropdown
                theme={theme}
                setValue={item => setSelectedLanguage(item)}
                options={options}
                label={selectedLanguage.label}
              />
            </>
          )}
        </View>
      </View>
    </>
  );
};

export default Configs;
