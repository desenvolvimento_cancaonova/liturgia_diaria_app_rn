import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  main: {
    paddingTop: 50,
    alignItems: 'center',
    flex: 1,
  },
  button: {
    width: 341,
    height: 60,
    borderColor: '#E5E5E5',
    borderRadius: 5,
    borderWidth: 2,
    backgroundColor: 'transparent',
    marginTop: 10,
    justifyContent: 'center',
    paddingLeft: 20,
    fontWeight: '700',
  },
  buttonText: {
    fontWeight: '300',
    color: '#253138',
    fontSize: 16,
  },
  buttonTextUnread: {
    fontWeight: '800',
    color: '#253138',
    fontSize: 16,
    fontFamily: fonts.BOLD,
  },
  buttonTextExpiration: {
    fontWeight: '800',
    color: '#0177BC',
    fontSize: 14,
    fontFamily: fonts.BOLD,
  },
  scrollViewContainer: {
    paddingRight: 30,
    paddingLeft: 30,
  },
});

export default styles;
