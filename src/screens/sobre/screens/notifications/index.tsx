import React, {useEffect, useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {screenView} from '~/services/firebase';
import {useTheme} from '~/context/themeContext';
import {useDate} from '~/context/dateContext';
import {SafeAreaView} from 'react-native';
import styles from './styles';
import Header from '~/components/header';
import Logo from '~/components/logo';
import {bodyColorLight, textColorLight} from '~/styles/theme';
import MessageModal from './messageModal';
import Notifications from '~/repository/database/model/notifications';

const NotificationsView = () => {
  const {theme} = useTheme();
  const {setPage} = useDate();

  const allNotifications = new Notifications().getAllNotifications();
  const [modalMessageVisible, setModalMessageVisible] = useState(false);
  const [notificationData, setNotificationData] = useState(null);

  useEffect(() => {
    screenView('Notificações');
    setPage('sobre_notificações');
  }, [setPage]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Header
          title="Notificações"
          onSelectDay={undefined}
          navigation={undefined}
          hasGoBack={true}
        />
        <View style={[styles.main, bodyColorLight(theme)]}>
          <Logo />
          <ScrollView style={styles.scrollViewContainer}>
            {allNotifications.length === 0 ? (
              <Text>Não há mensagens no momento</Text>
            ) : (
              allNotifications.map(notification => (
                <TouchableOpacity
                  key={notification.id}
                  style={styles.button}
                  onPress={() => {
                    setNotificationData(notification);
                    setModalMessageVisible(true);
                    new Notifications().markAsRead(notification.id);
                  }}>
                  <Text
                    style={[
                      notification.isRead
                        ? styles.buttonText
                        : styles.buttonTextUnread,
                      textColorLight(theme),
                    ]}>
                    {notification?.title}
                  </Text>
                  <Text
                    style={[
                      styles.buttonTextExpiration,
                      textColorLight(theme),
                    ]}>
                    Essa mensagem expira em {notification.expired_in}
                  </Text>
                </TouchableOpacity>
              ))
            )}
          </ScrollView>
        </View>
      </SafeAreaView>
      <MessageModal
        notificationData={notificationData}
        isVisible={modalMessageVisible}
        setVisible={setModalMessageVisible}
      />
    </>
  );
};

export default NotificationsView;
