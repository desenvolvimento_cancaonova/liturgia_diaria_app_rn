import React from 'react';
import {Modal, Text, TouchableOpacity, View} from 'react-native';
import {modal} from '~/interfaces/modal';
import styles from './styles';
import {WebView} from '~/components/webview';

const MessageModal = ({isVisible, setVisible, notificationData}: modal) => {
  return (
    <Modal
      visible={isVisible}
      animationType="slide"
      transparent={true}
      onRequestClose={() => {
        setVisible(!isVisible);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <View style={styles.title}>
            <Text style={styles.text}>{notificationData?.title}</Text>
            <TouchableOpacity
              onPress={() => {
                setVisible(!isVisible);
              }}>
              <Text style={styles.close}>X</Text>
            </TouchableOpacity>
          </View>
          <WebView content={notificationData?.content} isModal />
        </View>
      </View>
    </Modal>
  );
};

export default MessageModal;
