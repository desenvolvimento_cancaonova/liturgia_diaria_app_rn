import {StyleSheet, Dimensions} from 'react-native';
import fonts from '~/styles/fonts';

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000030',
  },
  modalView: {
    margin: 20,
    width: width,
    height: height * 0.5,
    paddingHorizontal: 15,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.24,
    shadowRadius: 5,
    elevation: 5,
  },
  title: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 2,
    flexDirection: 'row',
  },
  text: {
    fontFamily: fonts.BOLD,
    fontSize: 18,
    color: '#253138',
    paddingBottom: 18,
    paddingTop: 18,
    flex: 1,
  },
  close: {
    fontFamily: fonts.BOLD,
    fontSize: 18,
    color: '#253138',
    paddingBottom: 18,
    paddingTop: 18,
  },
});

export default styles;
