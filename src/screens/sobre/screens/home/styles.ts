import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  aboutContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  texts: {
    marginTop: 24,
    marginBottom: 90,
  },
  text: {
    color: '#194976',
    fontSize: 12,
    fontFamily: fonts.REGULAR,
  },
  button: {
    width: 341,
    height: 47,
    borderColor: '#E5E5E5',
    borderRadius: 5,
    borderWidth: 2,
    backgroundColor: 'transparent',
    marginTop: 10,
    justifyContent: 'center',
    paddingLeft: 20,
    fontWeight: '700',
  },
  buttonText: {
    fontWeight: '700',
    color: '#253138',
    fontSize: 16,
    fontFamily: fonts.BOLD,
  },
  dot: {
    position: 'absolute',
    right: 160,
    top: 16,
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'red',
  },
});

export default styles;
