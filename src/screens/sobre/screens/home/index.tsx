import React, {useEffect, useState} from 'react';
import {
  Linking,
  Platform,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Header from '~/components/header';
import Logo from '~/components/logo';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {navigationProps} from '~/interfaces/global';
import {predefinedEvent, screenView} from '~/services/firebase';
import {bodyColorLight, textColorLight} from '~/styles/theme';
import styles from './styles';
import Notifications from '~/repository/database/model/notifications';
import {useFocusEffect} from '@react-navigation/native';

const Home = ({navigation}: navigationProps) => {
  const {theme} = useTheme();
  const {setPage} = useDate();

  const [unreadMessages, setUnreadMessages] = useState();

  useEffect(() => {
    screenView('Sobre');
    setPage('Sobre');
  }, [setPage]);

  const whichDevice = () => {
    if (Platform.OS === 'android') {
      return 'https://play.google.com/store/apps/details?id=com.cancaonova.liturgia&hl=en&gl=US';
    }
    return 'https://apps.apple.com/pt/app/liturgia-di%C3%A1ria/id914272972';
  };

  useFocusEffect(() => {
    const result = new Notifications().existsUnreadMessage();
    setUnreadMessages(result);
  });

  return (
    <SafeAreaView style={styles.container}>
      <Header
        title="Sobre"
        onSelectDay={undefined}
        navigation={undefined}
        hasGoBack={undefined}
      />
      <View style={[styles.aboutContainer, bodyColorLight(theme)]}>
        <Logo />

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('Notifications');
            predefinedEvent('acessar_notificacoes', {
              item_name: 'acessar_notificacoes',
            });
          }}>
          <Text style={[styles.buttonText, textColorLight(theme)]}>
            Caixa de Entrada
          </Text>
          {unreadMessages && <View style={styles.dot} />}
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('Suggestions');
            predefinedEvent('enviar_sugestoes', {
              item_name: 'enviar_sugestoes',
            });
          }}>
          <Text style={[styles.buttonText, textColorLight(theme)]}>
            Enviar Sugestões
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('Configs');
            predefinedEvent('acessar_configuracoes', {
              item_name: 'acessar_configuracoes',
            });
          }}>
          <Text style={[styles.buttonText, textColorLight(theme)]}>
            Configurações
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            Linking.openURL(whichDevice());
            predefinedEvent('avaliar_app', {item_name: 'avaliar_app'});
          }}>
          <Text style={[styles.buttonText, textColorLight(theme)]}>
            Avalie o nosso aplicativo
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('Terms');
            predefinedEvent('acessar_termos', {item_name: 'Terms'});
          }}>
          <Text style={[styles.buttonText, textColorLight(theme)]}>
            Termos de uso
          </Text>
        </TouchableOpacity>
        <View style={styles.texts}>
          <Text style={[styles.text, textColorLight(theme)]}>
            Versão do aplicativo {DeviceInfo.getVersion()}
          </Text>
          <Text style={[styles.text, textColorLight(theme)]}>
            Copyright - Canção Nova
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Home;
