import {CommonActions} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from 'react-native';
import Header from '~/components/header';
import Logo from '~/components/logo';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {navigationProps} from '~/interfaces/global';
import {screenView} from '~/services/firebase';
import globalStyle, {
  bodyColorLight,
  bodyLightDark,
  darkText,
} from '~/styles/theme';
import {styles} from './styles';
import axios from 'axios';

const FormData = require('form-data');

const Suggestions = ({navigation}: navigationProps) => {
  const {theme} = useTheme();
  const [newSuggestion, setNewSuggestion] = useState();
  const [email, setEmail] = useState();
  const [name, setName] = useState();
  const {setPage} = useDate();
  const [loading, setLoading] = useState(false);
  const [errorMessages, setErrorMessages] = useState({
    name: '',
    email: '',
    suggestion: '',
  });

  const validateFields = () => {
    let errors = {name: '', email: '', suggestion: ''};
    let valid = true;

    const trimmedName = name?.trim();
    const trimmedEmail = email?.trim();
    const trimmedSuggestion = newSuggestion?.trim();

    if (!trimmedName) {
      errors.name = 'Nome é obrigatório';
      valid = false;
    } else if (trimmedName.length < 3) {
      errors.name = 'Nome deve ter pelo menos 3 caracteres';
      valid = false;
    }

    if (!trimmedEmail) {
      errors.email = 'E-mail é obrigatório';
      valid = false;
    } else if (!/\S+@\S+\.\S+/.test(trimmedEmail)) {
      errors.email = 'E-mail inválido';
      valid = false;
    }

    if (!trimmedSuggestion) {
      errors.suggestion = 'Sugestão é obrigatória';
      valid = false;
    } else if (trimmedSuggestion.length < 10) {
      errors.suggestion = 'Sugestão deve ter pelo menos 10 caracteres';
      valid = false;
    }

    setErrorMessages(errors);
    return valid;
  };

  useEffect(() => {
    screenView('Sugestões');
    setPage('Sobre_enviar_sugestoes');
  }, [setPage]);

  const sendMessage = async () => {
    if (!validateFields()) {
      return;
    }

    setLoading(true);

    try {
      let data = new FormData();
      data.append('username', name.trim());
      data.append('email', email.trim());
      data.append('suggestion', newSuggestion.trim());
      data.append('subject', 'Sugestão para o App Liturgia');

      await axios.post('app-email-redirect', data, {
        baseURL: 'https://www.cancaonova.com',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      Alert.alert('Sugestão', 'Sugestão enviada. Agradecemos sua ajuda!');

      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Main'}],
        }),
      );
    } catch (e) {
      console.log(JSON.stringify(e));
      Alert.alert(
        'Sugestão',
        'Houve um erro ao enviar sua sugestão, tente novamente.',
      );
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <Header
        title="Enviar sugestão"
        onSelectDay={undefined}
        navigation={undefined}
        hasGoBack={true}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{flex: 1}}
        keyboardVerticalOffset={30}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={[bodyColorLight(theme), styles.main]}>
          <Logo />
          <TextInput
            placeholderTextColor={
              theme !== 'Claro'
                ? globalStyle.lightText.color
                : globalStyle.darkText.color
            }
            multiline={true}
            numberOfLines={1}
            maxLength={100}
            style={{
              ...styles.input,
              ...{
                color:
                  theme !== 'Claro'
                    ? globalStyle.lightText.color
                    : globalStyle.darkText.color,
              },
            }}
            onChangeText={(s: string) => {
              setName(s);
              setErrorMessages({...errorMessages, name: ''});
            }}
            value={name}
            placeholder="Seu Nome"
          />
          {errorMessages.name && (
            <Text style={styles.errorText}>{errorMessages.name}</Text>
          )}
          <TextInput
            placeholderTextColor={
              theme !== 'Claro'
                ? globalStyle.lightText.color
                : globalStyle.darkText.color
            }
            multiline={true}
            numberOfLines={1}
            maxLength={100}
            style={{
              ...styles.input,
              ...{
                color:
                  theme !== 'Claro'
                    ? globalStyle.lightText.color
                    : globalStyle.darkText.color,
              },
            }}
            onChangeText={(s: string) => {
              setEmail(s);
              setErrorMessages({...errorMessages, email: ''});
            }}
            value={email}
            placeholder="Seu E-mail"
          />
          {errorMessages.email && (
            <Text style={styles.errorText}>{errorMessages.email}</Text>
          )}
          <TextInput
            placeholderTextColor={
              theme !== 'Claro'
                ? globalStyle.lightText.color
                : globalStyle.darkText.color
            }
            multiline={true}
            numberOfLines={10}
            maxLength={500}
            style={{
              ...styles.textArea,
              ...{
                color:
                  theme !== 'Claro'
                    ? globalStyle.lightText.color
                    : globalStyle.darkText.color,
              },
            }}
            onChangeText={(s: string) => {
              setNewSuggestion(s);
              setErrorMessages({...errorMessages, suggestion: ''});
            }}
            value={newSuggestion}
            placeholder="Digite aqui sua sugestão..."
          />
          {errorMessages.suggestion && (
            <Text style={styles.errorText}>{errorMessages.suggestion}</Text>
          )}
          <TouchableOpacity
            onPress={() => {
              sendMessage();
            }}
            style={[styles.button, bodyLightDark(theme)]}>
            <Text style={[darkText(theme), styles.buttonText]}>Enviar</Text>
          </TouchableOpacity>
          {loading && (
            <View style={styles.loadingContainer}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          )}
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Suggestions;
