import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  main: {flex: 1, alignItems: 'center', paddingTop: 40},
  button: {
    width: '90%',
    height: 47,
    marginTop: 40,
    justifyContent: 'center',
    fontWeight: '700',
    alignItems: 'center',
    borderRadius: 25,
  },
  textArea: {
    margin: 5,
    height: 200,
    width: '90%',
    borderWidth: 1,
    borderColor: '#E5E5E5',
    textAlignVertical: 'top',
  },
  input: {
    margin: 5,
    //height: 200,
    width: '90%',
    borderWidth: 1,
    borderColor: '#E5E5E5',
    textAlignVertical: 'center',
  },
  buttonText: {
    fontWeight: '700',
    fontSize: 16,
    fontFamily: fonts.BOLD,
  },
  errorText: {
    color: 'red',
    fontSize: 14,
    marginBottom: 10,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
