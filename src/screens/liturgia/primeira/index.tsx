import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Tts from 'react-native-tts';
import {WebView} from '~/components/webview';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {screenView} from '~/services/firebase';
import {bodyColorLight} from '~/styles/theme';
import Controls from '../../../components/controls';
import Top from '../../../components/top';
import Liturgia from '../../../repository/database/model/liturgia';
import styles from './styles';
import normalize from '~/utils/DynamicRatio';
import fonts from '~/styles/fonts';

const Leitura = () => {
  const webviewRef: any = useRef(null);
  const [tab] = useState(0);
  const {theme} = useTheme();
  const {liturgiaModel, setDates, setPage, retry} = useDate();
  const [loading, setLoading] = useState(false);

  const getDays = useCallback(() => {
    const dates: any[] = [];
    const liturgia = new Liturgia().getAll();

    liturgia.forEach(item => dates.push(item.dia));
    setDates(dates);
  }, [setDates]);

  useFocusEffect(
    React.useCallback(() => {
      getDays();
      setPage('Liturgia');
      screenView('Primeira leitura');
    }, [getDays, setPage]),
  );

  useEffect(() => {
    if (!liturgiaModel) {
      retry();
    }
  }, [liturgiaModel]);

  useEffect(() => {
    if (!liturgiaModel) {
      retry();
    }
  }, []);

  useEffect(() => {
    Tts.getInitStatus().then(
      () => {
        // ...
      },
      err => {
        if (err.code === 'no_engine') {
          Tts.requestInstallEngine();
        }
      },
    );
  }, []);

  return (
    <SafeAreaView style={[styles.container, bodyColorLight(theme)]}>
      <ScrollView>
        {liturgiaModel && (
          <>
            <Top
              dia_formatado={liturgiaModel.dia_formatado}
              mes={liturgiaModel.mes}
              ano={liturgiaModel.ano}
              color={liturgiaModel.cor}
              title={liturgiaModel.titulo}
            />

            <Controls
              tab={tab}
              type="liturgia"
              model={liturgiaModel}
              handleCopy={() => new Liturgia().textoToCopy(liturgiaModel, 0)}
              url={liturgiaModel?.url}
              title={'Liturgia'}
              zoomIn={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomIn();
                }
              }}
              zoomOut={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomOut();
                }
              }}
              audioUrl={liturgiaModel.audio_leitura1}
            />
          </>
        )}

        {liturgiaModel ? (
          <View style={[styles.webview]}>
            <WebView
              ref={webviewRef}
              content={liturgiaModel.leitura1 + '<p>&nbsp;</p>'}
            />
          </View>
        ) : (
          <View style={{alignItems: 'center'}}>
            <Text
              style={{marginTop: 20, alignSelf: 'center', color: '#333333'}}>
              Conteúdo não disponível ou ainda não baixado.
            </Text>
            <TouchableOpacity
              onPress={retry}
              style={{
                borderRadius: normalize(20),
                paddingVertical: normalize(10),
                paddingHorizontal: normalize(14),
                marginTop: normalize(20),
                marginBottom: normalize(20),
                backgroundColor: '#194976',
              }}>
              <Text
                style={{
                  fontSize: normalize(14),
                  color: '#FFF',
                  fontFamily: fonts.REGULAR,
                }}>
                Tentar baixar conteúdo agora
              </Text>
            </TouchableOpacity>
            {loading && <ActivityIndicator size={'small'} color={'#AAAAAA'} />}
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Leitura;
