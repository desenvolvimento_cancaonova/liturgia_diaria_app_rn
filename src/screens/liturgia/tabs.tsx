import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import globalStyle from '~/styles/theme';
import normalize from '~/utils/DynamicRatio';
import Evangelho from './evangelho';
import Primeira from './primeira';
import Salmos from './salmos';
import SegundaLeitura from './segundo';

const Tab = createMaterialTopTabNavigator();

export function MyTabs() {
  const {liturgiaModel} = useDate();
  const {theme} = useTheme();

  const tabOptions = {
    tabBarActiveTintColor: '#fff',
    tabBarIndicatorStyle: {
      backgroundColor: '#fff',
    },
    tabBarLabelStyle: {
      fontSize:
        liturgiaModel !== undefined &&
        liturgiaModel.leitura2 &&
        liturgiaModel.leitura2 !== ''
          ? normalize(10)
          : normalize(13),
    },
    tabBarStyle: {
      backgroundColor:
        theme !== 'Totalmente escuro'
          ? globalStyle.mediumBody.backgroundColor
          : globalStyle.darkBody.backgroundColor,
    },
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    tabLabelContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
    },
    tabLabelText: {
      fontSize:
        liturgiaModel !== undefined &&
        liturgiaModel.leitura2 &&
        liturgiaModel.leitura2 !== ''
          ? normalize(10)
          : normalize(14),
    },
    tabLabelFocused: {
      color: '#fff',
    },
    tabLabelUnfocused: {
      color: '#b3b3b3',
    },
  });

  const TabLabel = ({focused, text}) => {
    return (
      <View style={styles.tabLabelContainer}>
        <Text
          numberOfLines={1}
          ellipsizeMode="tail"
          style={[
            styles.tabLabelText,
            focused ? styles.tabLabelFocused : styles.tabLabelUnfocused,
          ]}>
          {text}
        </Text>
      </View>
    );
  };

  return (
    <Tab.Navigator screenOptions={tabOptions}>
      <Tab.Screen
        name="1 Leitura"
        component={Primeira}
        options={{
          tabBarLabel: ({focused}) => (
            <TabLabel focused={focused} text="1 LEITURA" />
          ),
        }}
      />
      <Tab.Screen
        name="Salmos"
        component={Salmos}
        options={{
          tabBarLabel: ({focused}) => (
            <TabLabel focused={focused} text="SALMOS" />
          ),
        }}
      />
      {liturgiaModel !== undefined && liturgiaModel?.leitura2 !== '' && (
        <Tab.Screen
          name="2 Leitura"
          component={SegundaLeitura}
          options={{
            tabBarLabel: ({focused}) => (
              <TabLabel focused={focused} text="2 LEITURA" />
            ),
          }}
        />
      )}
      <Tab.Screen
        name="Evangelho"
        component={Evangelho}
        options={{
          tabBarLabel: ({focused}) => (
            <TabLabel focused={focused} text="EVANGELHO" />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
