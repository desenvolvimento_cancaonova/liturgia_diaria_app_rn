import React, {useRef, useState, useEffect} from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import PlayerHomilia from '~/components/playerHomilia';
import {WebView} from '~/components/webview';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import Liturgia from '~/repository/database/model/liturgia';
import {bodyColorLight} from '~/styles/theme';
import Controls from '../../../components/controls';
import Top from '../../../components/top';
import styles from './styles';
import {screenView} from '~/services/firebase';

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const Salmos = () => {
  const webviewRef: any = useRef(null);
  const {liturgiaModel} = useDate();
  const [tab] = useState(2);
  const {theme} = useTheme();

  const initEventSalmos = () => {
    try {
      screenView('Salmos');
    } catch (error) {
      console.log('initEventSalmos error: ', error);
    }
  };
  useEffect(() => {
    initEventSalmos();
  }, []);

  return (
    <SafeAreaView style={[styles.container, bodyColorLight(theme)]}>
      <ScrollView>
        {liturgiaModel && (
          <>
            <Top
              dia_formatado={liturgiaModel.dia_formatado}
              mes={liturgiaModel.mes}
              ano={liturgiaModel.ano}
              color={liturgiaModel.cor}
              title={liturgiaModel.titulo}
            />
            <Controls
              type="liturgia"
              model={liturgiaModel}
              handleCopy={() => new Liturgia().textoToCopy(liturgiaModel, 2)}
              tab={tab}
              url={liturgiaModel?.url}
              title={'Liturgia'}
              zoomIn={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomIn();
                }
              }}
              zoomOut={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomOut();
                }
              }}
              audioUrl={liturgiaModel.audio_salmo}
            />
          </>
        )}

        {liturgiaModel ? (
          <View style={[styles.webview]}>
            <WebView
              ref={webviewRef}
              content={liturgiaModel.salmo + '<p>&nbsp;</p>'}
            />
          </View>
        ) : (
          <Text style={{marginTop: 20, alignSelf: 'center', color: '#CCCCCC'}}>
            Sem conteúdo para essa data.
          </Text>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Salmos;
