import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import PlayerHomilia from '~/components/playerHomilia';
import {WebView} from '~/components/webview';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {screenView} from '~/services/firebase';
import {bodyColorLight} from '~/styles/theme';
import Controls from '../../../components/controls';
import Top from '../../../components/top';
import Liturgia from '../../../repository/database/model/liturgia';
import styles from './styles';

const SegundaLeitura = () => {
  const webviewRef: any = useRef(null);
  const [tab] = useState(1);

  const {theme} = useTheme();
  const {liturgiaModel, setDates, setPage} = useDate();

  const getDays = useCallback(() => {
    const dates: any[] = [];
    const homilia = new Liturgia().getAll();
    homilia.forEach(item => dates.push(item.dia));
    setDates(dates);
  }, [setDates]);

  useFocusEffect(
    React.useCallback(() => {
      getDays();
      setPage('Liturgia');
      screenView('Segunda leitura');
    }, [getDays]),
  );

  return (
    <SafeAreaView style={[styles.container, bodyColorLight(theme)]}>
      <ScrollView>
        {liturgiaModel && (
          <>
            <Top
              dia_formatado={liturgiaModel.dia_formatado}
              mes={liturgiaModel.mes}
              ano={liturgiaModel.ano}
              color={liturgiaModel.cor}
              title={liturgiaModel.titulo}
            />
            <Controls
              tab={tab}
              type="liturgia"
              model={liturgiaModel}
              handleCopy={() => new Liturgia().textoToCopy(liturgiaModel, 1)}
              url={liturgiaModel?.url}
              title={'Liturgia'}
              zoomIn={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomIn();
                }
              }}
              zoomOut={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomOut();
                }
              }}
              audioUrl={liturgiaModel.audio_leitura2}
            />
          </>
        )}

        {liturgiaModel ? (
          <>
            <View style={[styles.webview]}>
              <WebView
                ref={webviewRef}
                content={liturgiaModel.leitura2 + '<p>&nbsp;</p>'}
              />
            </View>
          </>
        ) : (
          <Text style={{marginTop: 20, alignSelf: 'center', color: '#CCCCCC'}}>
            Sem conteúdo para essa data.
          </Text>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default SegundaLeitura;
