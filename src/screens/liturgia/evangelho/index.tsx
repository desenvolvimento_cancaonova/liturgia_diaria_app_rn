import React, {useRef, useState, useEffect} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import PlayerHomilia from '~/components/playerHomilia';
import {WebView} from '~/components/webview';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import Liturgia from '~/repository/database/model/liturgia';
import {bodyColorLight} from '~/styles/theme';
import Controls from '../../../components/controls';
import Top from '../../../components/top';
import {screenView} from '~/services/firebase';

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    height: '100%',
  },
  webview: {
    flex: 1,
    paddingHorizontal: 17,
  },
});

const Evangelho = () => {
  const webviewRef: any = useRef(null);
  const {liturgiaModel} = useDate();
  const [tab] = useState(3);
  const {theme} = useTheme();

  const initEventEvangelho = () => {
    try {
      screenView('Evangelho');
    } catch (error) {
      console.log('initEventEvangelho error: ', error);
    }
  };
  useEffect(() => {
    initEventEvangelho();
  }, []);

  return (
    <SafeAreaView style={[styles.container, bodyColorLight(theme)]}>
      <ScrollView>
        {liturgiaModel && (
          <>
            <Top
              dia_formatado={liturgiaModel.dia_formatado}
              mes={liturgiaModel.mes}
              ano={liturgiaModel.ano}
              color={liturgiaModel.cor}
              title={liturgiaModel.titulo}
            />
            <Controls
              type="liturgia"
              model={liturgiaModel}
              handleCopy={() => new Liturgia().textoToCopy(liturgiaModel, 3)}
              tab={tab}
              url={liturgiaModel?.url}
              title={'Liturgia'}
              zoomIn={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomIn();
                }
              }}
              zoomOut={() => {
                if (webviewRef.current) {
                  webviewRef.current.zoomOut();
                }
              }}
              audioUrl={liturgiaModel.audio_evangelho}
            />
          </>
        )}

        {liturgiaModel ? (
          <>
            <View style={[styles.webview]}>
              <WebView
                ref={webviewRef}
                content={liturgiaModel.evangelho + '<p>&nbsp;</p>'}
              />
            </View>
          </>
        ) : (
          <Text style={{marginTop: 20, alignSelf: 'center', color: '#CCCCCC'}}>
            Sem conteúdo para essa data.
          </Text>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Evangelho;
