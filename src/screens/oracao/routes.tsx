import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';
import OracaoTela from './screens/Oracaotela';
import OracoesCategorias from './screens/Oracoescategorias';
import OracoesLista from './screens/Oracoeslista';

const Stack = createNativeStackNavigator();

export default function Routes() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="OracoesCategorias" component={OracoesCategorias} />
      <Stack.Screen name="OracoesLista" component={OracoesLista} />
      <Stack.Screen name="OracaoTela" component={OracaoTela} />
    </Stack.Navigator>
  );
}
