import {useFocusEffect, useRoute} from '@react-navigation/native';
import React, {useEffect, useRef, useState} from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import Controls from '~/components/controls';
import Header from '~/components/header';
import {WebView} from '~/components/webview';
import {useTheme} from '~/context/themeContext';
import OracaoModel from '~/repository/database/model/oracao';
import {screenView} from '~/services/firebase';
import {bodyColorLight, textColorLight} from '~/styles/theme';
import {removeTagBody} from '~/utils/newHtml';
import styles from '../../styles';

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const OracaoTela = () => {
  const webviewRef: any = useRef();
  const {theme} = useTheme();
  const {params} = useRoute();
  const oracaoId = params.id;
  const nome = params.name;
  const [oracaoModel, setOracaoModel] = useState(null);

  useEffect(() => {
    const oracao = new OracaoModel().get(oracaoId);

    setOracaoModel(oracao);
  }, [oracaoId]);

  useFocusEffect(
    React.useCallback(() => {
      oracaoModel &&
        screenView(`Oracoes_${nome}${oracaoModel.titulo}`.substr(0, 40));
    }, []),
  );

  return (
    <>
      {oracaoModel && (
        <SafeAreaView style={[{height: '100%'}, bodyColorLight(theme)]}>
          <Header hasGoBack={true} title="Orações" />
          <ScrollView>
            {oracaoModel && (
              <SafeAreaView>
                <View style={styles.containerTop}>
                  <Text style={[styles.title, textColorLight(theme)]}>
                    {oracaoModel.titulo}
                  </Text>
                </View>
                <Controls
                  type="oracao"
                  model={oracaoModel}
                  url={oracaoModel.url}
                  handleCopy={() => new OracaoModel().textoToCopy(oracaoModel)}
                  title={oracaoModel.titulo}
                  zoomIn={() => {
                    if (webviewRef.current) {
                      webviewRef.current.zoomIn();
                    }
                  }}
                  zoomOut={() => {
                    if (webviewRef.current) {
                      webviewRef.current.zoomOut();
                    }
                  }}
                />
                {/* <Player type="oracao" model={oracaoModel} /> */}
                <View style={{paddingHorizontal: 17, flex: 1}}>
                  <WebView
                    oracao
                    ref={webviewRef}
                    content={removeTagBody(oracaoModel.texto) + '<p>&nbsp;</p>'}
                  />
                </View>
              </SafeAreaView>
            )}
          </ScrollView>
        </SafeAreaView>
      )}
    </>
  );
};

export default OracaoTela;
