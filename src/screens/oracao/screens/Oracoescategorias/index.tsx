import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Header from '~/components/header';
import {useDate} from '~/context/dateContext';
import {useRequest} from '~/context/requestContext';
import {useTheme} from '~/context/themeContext';
import {NavigationProps} from '~/interfaces/global';
import OracaoCategoriaModel from '~/repository/database/model/oracaoCategoria';
import {screenView} from '~/services/firebase';
import {bodyColorLight, bodyColorMedium, textColorLight} from '~/styles/theme';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: '100%',
  },
  listaItens: {
    padding: 10,
    height: '100%',
    width: '100%',
  },
  textoBotao: {
    color: '#253138',
    fontSize: 20,
    fontWeight: '400',
  },
  botao: {
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
});

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const OracoesCategorias = ({navigation}: NavigationProps) => {
  const [oracaoCategoriaModel, setOracaoCategoriaModel] = useState(null);
  const {theme} = useTheme();
  const {getOracao} = useRequest();
  const {setDates, setDate, setPage} = useDate();

  useFocusEffect(
    React.useCallback(() => {
      screenView('Oracoes');
      setPage('Oracoes');
    }, [setDate, setDates, setPage]),
  );

  const retryOracao = useCallback(async () => {
    await getOracao();
    setTimeout(() => {
      const oracaoCategoria = new OracaoCategoriaModel().all();
      setOracaoCategoriaModel(oracaoCategoria);
    }, 1500);
  }, [getOracao]);

  useEffect(() => {
    const oracaoCategoria = new OracaoCategoriaModel().all();
    if (oracaoCategoria && oracaoCategoria.length > 0) {
      setOracaoCategoriaModel(oracaoCategoria);
      return;
    }
    retryOracao();
  }, [retryOracao]);

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.botao}
        onPress={() => {
          navigation.navigate('OracoesLista', {
            idCategoria: item.id,
            nomeCategoria: item.nome,
          });
        }}>
        <Text style={[styles.textoBotao, textColorLight(theme)]} key={index}>
          {item.nome}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={[styles.container, bodyColorMedium(theme)]}>
      <Header
        title="Orações"
        navigation={navigation}
        onSelectDay={undefined}
        hasGoBack={undefined}
      />
      {oracaoCategoriaModel ? (
        <FlatList
          data={oracaoCategoriaModel}
          keyExtractor={(_, index) => index.toString()}
          renderItem={({item, index}) => renderItem({item, index})}
          scrollEventThrottle={16}
          style={[styles.listaItens, bodyColorLight(theme)]}
        />
      ) : (
        <ActivityIndicator style={{flex: 1}} size="large" color="#194976" />
      )}
    </SafeAreaView>
  );
};

export default OracoesCategorias;
