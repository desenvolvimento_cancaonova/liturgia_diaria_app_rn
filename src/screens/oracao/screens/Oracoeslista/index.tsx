import {useFocusEffect, useRoute} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {FlatList, SafeAreaView, Text, TouchableOpacity} from 'react-native';
import Header from '~/components/header';
import {useTheme} from '~/context/themeContext';
import {NavigationProps} from '~/interfaces/global';
import OracaoModel from '~/repository/database/model/oracao';
import {screenView} from '~/services/firebase';
import {
  bodyColorLight,
  textColorLight,
  textColorLightGray,
} from '~/styles/theme';
import {removeTagBody} from '~/utils/newHtml';
import removeHTML from '~/utils/RemoveHtml';
import styles from '../../styles';

type ScrollEvent = {
  nativeEvent: {
    contentOffset: {y: number};
  };
};

const OracoesLista = ({navigation}: NavigationProps) => {
  const {params} = useRoute();
  const id = params.idCategoria;
  const nome = params.nomeCategoria;
  const [oracaoCategoriaModel, setOracaoCategoriaModel] = useState(null);
  const {theme} = useTheme();

  useEffect(() => {
    const oracao = new OracaoModel().all();
    if (oracao) {
      const oracaoFiltered = oracao.filter(item => item.categoria.id === id);
      setOracaoCategoriaModel(oracaoFiltered);
    }
  }, [id]);

  useFocusEffect(
    React.useCallback(() => {
      screenView(`Oracoes_${nome}`);
    }, []),
  );

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={[styles.botao, bodyColorLight(theme)]}
        onPress={() => {
          navigation.navigate('OracaoTela', {id: item.id, name: nome});
        }}>
        <Text
          key={index}
          style={[styles.textoBotaoCategories, textColorLight(theme)]}>
          {item.titulo}
        </Text>
        <Text
          numberOfLines={2}
          style={[styles.description, textColorLightGray(theme)]}>
          {removeHTML(removeTagBody(item.texto))}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView
      style={{backgroundColor: bodyColorLight(theme), height: '100%'}}>
      <Header
        hasGoBack={true}
        title={nome.substr(0, 7)}
        navigation={navigation}
      />
      <FlatList
        data={oracaoCategoriaModel}
        keyExtractor={(_, index) => index.toString()}
        renderItem={({item, index}) => renderItem({item, index})}
        scrollEventThrottle={16}
        style={[styles.listaItens, bodyColorLight(theme)]}
      />
    </SafeAreaView>
  );
};

export default OracoesLista;
