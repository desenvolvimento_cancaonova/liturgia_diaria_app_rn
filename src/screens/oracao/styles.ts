import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';
import normalize from '~/utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {backgroundColor: '#fff', height: '100%'},
  containerTop: {
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingTop: 32,
    marginBottom: normalize(16),
  },
  color: {fontFamily: fonts.BOLD},
  title: {
    marginBottom: 10,
    fontSize: normalize(24),
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: fonts.BOLD,
    color: '#000',
  },
  listaItens: {
    padding: 20,
    height: '100%',
    width: '100%',
  },
  textoBotao: {color: '#253138', fontSize: 20, fontWeight: '400'},
  botao: {
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    padding: 100,
  },
  textoBotaoCategories: {
    color: '#253138',
    fontSize: 18,
    fontFamily: fonts.REGULAR,
    textAlign: 'left',
  },
  description: {
    maxWidth: 300,
    textAlign: 'left',
    fontFamily: fonts.ITALIC,
    color: '#999999',
  },
  botao: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '100%',
    marginBottom: normalize(24),
  },
});
export default styles;
