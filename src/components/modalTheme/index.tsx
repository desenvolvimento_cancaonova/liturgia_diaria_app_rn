import React, {useState} from 'react';
import {Modal, Text, View} from 'react-native';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {useTheme} from '~/context/themeContext';
import {modal} from '~/interfaces/modal';
import Load from '../load';
import styles from './styles';

const ModalTheme = ({isVisible, setVisible}: modal) => {
  const {theme, setTheme, changeTheme} = useTheme();
  const [show, setShow] = useState(false);

  const load = () => {
    setVisible(false);
    setShow(true);
    setTimeout(() => {
      setShow(false);
    }, 750);
  };

  const checkbox = (title: string) => (
    <View style={styles.containerCheck}>
      <BouncyCheckbox
        isChecked={theme === title}
        size={25}
        fillColor="#194976"
        disableBuiltInState
        unfillColor="#fff"
        text={title}
        iconStyle={styles.checkIcon}
        textStyle={styles.checkText}
        onPress={_ => {
          setTheme(title);
          changeTheme(title);
          load();
        }}
      />
    </View>
  );

  return (
    <>
      <Modal
        visible={isVisible}
        animationType="slide"
        transparent={true}
        onRequestClose={() => {
          setVisible(!isVisible);
        }}>
        <View
          style={styles.centeredView}
          onStartShouldSetResponder={() => setVisible(false)}>
          <View
            style={styles.modalView}
            onStartShouldSetResponder={() => setVisible(false)}>
            <View style={styles.title}>
              <Text style={styles.text}>Tema</Text>
            </View>
            {checkbox('Claro')}
            {checkbox('Pouco escuro')}
            {checkbox('Totalmente escuro')}
          </View>
        </View>
      </Modal>
      {show && <Load />}
    </>
  );
};

export default ModalTheme;
