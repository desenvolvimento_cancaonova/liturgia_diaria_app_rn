import {StyleSheet} from 'react-native';
import fonts from '~/styles/fonts';

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000030',
  },
  modalView: {
    margin: 20,
    width: 280,
    height: 224,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.24,
    shadowRadius: 5,
    elevation: 5,
  },
  title: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 2,
  },
  text: {
    fontFamily: fonts.BOLD,
    fontSize: 18,
    color: 'black',
    paddingBottom: 18,
    paddingTop: 18,
    paddingLeft: 24,
  },
  containerCheckBox: {
    paddingLeft: 24,
    flex: 1,
    justifyContent: 'space-evenly',
    flexDirection: 'column',
  },
  containerCheck: {paddingLeft: 28, justifyContent: 'space-evenly', flex: 1},
  checkIcon: {
    borderColor: '#C4C4C4',
    borderWidth: 2,
  },
  checkText: {
    fontFamily: fonts.REGULAR,
    color: '#000',
    textDecorationLine: 'none',
  },
});

export default styles;
