import React, {useState} from 'react';

import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import normalize from '~/utils/DynamicRatio';

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    borderColor: '#E5E5E5',
    borderWidth: 1,
    marginTop: 8,
    marginBottom: 242,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    position: 'absolute',
    height,
    width,
    backgroundColor: 'transparent',
    borderWidth: 3,
    justifyContent: 'center',
  },
  picker: {
    // position: 'absolute',
    bottom: normalize(-12),
    alignSelf: 'center',
    alignItems: 'center',
    shadowColor: '#171717',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    backgroundColor: '#fff',
    width: normalize(350),
    minHeight: normalize(100),
    justifyContent: 'center',
    borderRadius: 5,
  },
  item: {
    borderBottomWidth: 1,
    borderColor: '#a38b8b',
    width: normalize(310),
    alignItems: 'center',
    padding: 2,
  },
});
type Options = {
  label: string;
  value: string | number;
};

interface DropdownProps {
  label: string;
  setValue: (item: Options) => void;
  options: Options[];
  theme: string;
}

const Dropdown: React.FC<DropdownProps> = ({
  label,
  setValue,
  options,
  theme,
}) => {
  const [show, setShow] = useState(false);

  return (
    <TouchableOpacity onPress={() => setShow(!show)} style={styles.container}>
      <Text
        style={{
          ...{alignSelf: 'flex-start', padding: normalize(10)},
          ...{color: theme === 'Claro' ? '#7f8c8d' : '#C1C1C1'},
        }}>
        {label || options[0].label}
      </Text>
      {show && (
        <View style={styles.content}>
          <View
            style={{
              ...styles.picker,
              ...(theme === 'Pouco escuro' && {backgroundColor: '#194976'}),
              ...(theme === 'Totalmente escuro' && {
                backgroundColor: '#1d1f1f',
              }),
            }}>
            <ScrollView>
              {options.map(item => (
                <TouchableOpacity
                  style={styles.item}
                  onPress={() => {
                    setValue(item);
                    setShow(false);
                  }}
                  key={String(item.value)}>
                  <Text
                    style={{color: theme === 'Claro' ? '#7f8c8d' : '#C1C1C1'}}>
                    {item.label}
                  </Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </View>
      )}
    </TouchableOpacity>
  );
};
export default Dropdown;
