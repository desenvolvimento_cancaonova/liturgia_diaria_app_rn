import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {useTheme} from '~/context/themeContext';

const styles = StyleSheet.create({
  lightLogo: {
    marginTop: 10,
    marginBottom: 13,
    width: 111,
    height: 75,
    resizeMode: 'cover',
  },
  darkLogo: {
    marginTop: 10,
    marginBottom: 13,
    width: 90,
    height: 75,
    resizeMode: 'cover',
  },
});

const Logo = () => {
  const {theme} = useTheme();
  return theme === 'Claro' ? (
    <Image
      style={styles.lightLogo}
      source={require('../../assets/images/menu/logoCancaoNova.png')}
    />
  ) : (
    <Image
      style={styles.darkLogo}
      source={require('../../assets/images/logoPreload/logo_cancao_nova.png')}
    />
  );
};

export default Logo;
