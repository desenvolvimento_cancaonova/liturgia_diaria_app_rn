import React, {useState} from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Linking,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {useTheme} from '~/context/themeContext';
import {predefinedEvent} from '~/services/firebase';
import fonts from '~/styles/fonts';
import {bodyLightBlue, textColorDark} from '~/styles/theme';
import normalize from '../../utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: normalize(17),
    right: normalize(-10),
  },
  button: {
    height: normalize(60),
    backgroundColor: '#194976',
    borderRadius: 120,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: normalize(20),
  },
  text: {
    fontFamily: fonts.BOLD,
    fontSize: normalize(18),
    color: '#FFFFFF',
  },

  image: {
    width: normalize(26),
    height: normalize(26),
    resizeMode: 'cover',
  },
});

const ColaboreButton = () => {
  const {theme} = useTheme();
  const {height, width} = Dimensions.get('window');
  const buttonSizeAnimate = new Animated.Value(1);
  const opacity = new Animated.Value(0);
  const textWidth = new Animated.Value(0);
  const [animated, setAnimated] = useState(false);
  const animatedStyle = {
    width: buttonSizeAnimate.interpolate({
      inputRange: [0, 1],
      outputRange: ['15%', '54%'],
    }),
  };

  const imageAnimatedStyle = {
    right: textWidth.interpolate({
      inputRange: [0, 1],
      outputRange: ['-1%', '30%'],
      extrapolate: 'clamp',
    }),
  };

  const textAnimatedStyle = {
    width: textWidth.interpolate({
      inputRange: [0, 2],
      outputRange: ['0%', '100%'],
    }),
    opacity: opacity.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    }),
    right: textWidth.interpolate({
      inputRange: [0, 1],
      outputRange: ['10%', '-60%'],
      extrapolate: 'clamp',
    }),
  };

  const handleColaboreAnimate = () => {
    if (!animated) {
      Animated.timing(buttonSizeAnimate, {
        toValue: 2,
        duration: 600,
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();

      Animated.timing(textWidth, {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();

      Animated.timing(opacity, {
        toValue: 1,
        duration: 1300,
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();

      setTimeout(() => {
        setAnimated(true);
      }, 1000);
    } else {
      Linking.openURL('https://secure.cancaonova.com/da/?app=liturgia');
    }
  };

  const handleColaboreAnimateDisable = () => {
    Animated.timing(buttonSizeAnimate, {
      toValue: 1,
      duration: 600,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start();

    Animated.timing(textWidth, {
      toValue: 0,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start();

    Animated.timing(opacity, {
      toValue: 0,
      duration: 1300,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start();

    setTimeout(() => {
      setAnimated(false);
    }, 300);
  };

  return (
    <>
      {animated && (
        <TouchableWithoutFeedback onPress={handleColaboreAnimateDisable}>
          <View
            style={{
              position: 'absolute',
              width,
              height,
              left: 0,
              top: 0,
            }}
          />
        </TouchableWithoutFeedback>
      )}
      <View style={[styles.container]}>
        <TouchableWithoutFeedback
          onPress={() => {
            handleColaboreAnimate();
            predefinedEvent('Doacoes');
          }}>
          <Animated.View
            style={[styles.button, animatedStyle, bodyLightBlue(theme)]}>
            <Animated.Image
              style={[styles.image, imageAnimatedStyle]}
              source={
                theme !== 'Claro'
                  ? require('../../assets/images/colaboreBlue.png')
                  : require('../../assets/images/colabore.png')
              }
            />
            <Animated.Text
              style={[styles.text, textAnimatedStyle, textColorDark(theme)]}>
              COLABORE
            </Animated.Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </>
  );
};
export default ColaboreButton;
