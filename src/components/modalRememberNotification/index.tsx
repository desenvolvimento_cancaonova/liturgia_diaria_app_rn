import moment from 'moment';
import 'moment/locale/pt-br';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  Modal,
  Text,
  TouchableOpacity,
  View,
  Linking,
  Platform,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import PushNotification from 'react-native-push-notification';
import {modal} from '../../interfaces/modal';
import {styles} from './styles';

const ModalRememberNotification = ({isVisible, setVisible}: modal) => {
  const [time, setTime] = useState('');
  const [info, setInfo] = useState([]);
  const [alarmPermission, setAlarmPermission] = useState(false);
  const [warningMessage, setWarningMessage] = useState('');

  // useEffect(() => {
  //   setTime(moment().format('LT'));
  // }, []);

  const createChannels = () => {
    PushNotification.createChannel(
      {
        channelId: 'Liturgia',
        channelName: 'Leitura do dia',
      },
      created => created,
    );
  };

  const loadNotify = () =>
    PushNotification.getScheduledLocalNotifications(nots => {
      setInfo(nots);
    });

  useEffect(() => {
    loadNotify();
    return createChannels();
  }, []);

  const clearNotification = () => {
    PushNotification.cancelAllLocalNotifications();
    setInfo([]);
    setTime('');
  };

  const handleNotification = () => {
    if (!time || time.length !== 5) {
      Alert.alert('Erro', 'Por favor insira um horário válido.');
      return;
    }

    const dateNow = moment();
    setVisible(false);
    PushNotification.cancelAllLocalNotifications();
    const timeNewNotification = moment(time, 'HH:mm').diff(
      moment(dateNow, 'HH:mm'),
    );
    if (timeNewNotification > 0) {
      PushNotification.localNotificationSchedule({
        channelId: 'Liturgia',
        title: 'Liturgia Diária',
        message: 'Hora da leitura do dia',
        date: new Date(Date.now() + timeNewNotification),
        allowWhileIdle: false,
        repeatType: 'day',
        repeatTime: 1,
      });
    } else {
      const newTimeNewNotification = moment(time, 'HH:mm')
        .add(1, 'day')
        .diff(dateNow);
      PushNotification.localNotificationSchedule({
        date: new Date(Date.now() + newTimeNewNotification),
        channelId: 'Liturgia',
        title: 'Liturgia Diária',
        message: 'Hora da leitura do dia',
        allowWhileIdle: false,
        repeatType: 'day',
        repeatTime: 1,
      });
    }

    setTimeout(loadNotify, 1000);
  };

  const onChangeTime = (str: string) => {
    if (str.length === 5) {
      const [hour, minute] = str.split(':');
      if (Number(hour) > 23 || Number(minute) > 59) {
        Alert.alert(
          'Liturgia Diária',
          `O horário digitado ('${str}') não parece ser válido. Tente novamente.`,
        );
        str = '';
      }
    }

    setTime(str);
  };

  const openSettings = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL('app-settings:');
    } else {
      Linking.openSettings();
    }
    setVisible(!isVisible);
  };

  const handleWarningMessage = () => {
    setWarningMessage(
      ' Aperte "Limpar Notificações" antes de agendar um novo lembrete',
    );
  };

  function formatTime(dateString: string) {
    return moment(dateString).local().format('HH:mm');
  }

  useEffect(() => {
    PushNotification.checkPermissions(permissions => {
      if (permissions.alert) {
        setAlarmPermission(true);
      } else {
        setAlarmPermission(false);
      }
    });
    if (info.length > 0) {
      setTime(formatTime(info[0].date));
    }
    setWarningMessage('');
  }, [isVisible, info]);

  return (
    <Modal
      visible={isVisible}
      animationType="slide"
      transparent={true}
      onRequestClose={() => {
        setVisible(!isVisible);
      }}>
      {alarmPermission ? (
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.title}>
              <Text style={styles.text}>Agendar lembrete</Text>
              <TouchableOpacity
                onPress={() => {
                  setVisible(!isVisible);
                }}>
                <Text style={styles.close}>X</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.description}>
              Receba uma notificação para ler o conteúdo litúrgico do
              aplicativo.
            </Text>
            <View style={styles.containerInputs}>
              {info.length === 0 ? (
                <View style={styles.mainInputs}>
                  <TextInputMask
                    style={styles.inputDate}
                    options={{
                      mask: '99:99',
                    }}
                    type={'custom'}
                    value={time}
                    placeholder="hh:mm"
                    onChangeText={onChangeTime}
                    keyboardType="numeric"
                  />
                  <View>
                    <Image source={require('../../assets/images/clock.png')} />
                  </View>
                </View>
              ) : (
                <>
                  <TouchableOpacity
                    onPress={handleWarningMessage}
                    style={styles.mainInputs}>
                    <Text style={styles.inputDate}>{time}</Text>
                    <Image source={require('../../assets/images/clock.png')} />
                  </TouchableOpacity>
                  {warningMessage && (
                    <Text style={styles.errorText}>{warningMessage}</Text>
                  )}
                </>
              )}
            </View>
            <View style={styles.modalTime}>
              {info.length === 0 ? (
                <TouchableOpacity
                  onPress={handleNotification}
                  style={styles.buttonCreateNotification}>
                  <Text style={styles.textButtonCreateNotification}>
                    AGENDAR
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={clearNotification}
                  style={styles.buttonCreateNotification}>
                  <Text style={styles.textButtonCreateNotification}>
                    LIMPAR NOTIFICAÇÕES
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      ) : (
        <View style={styles.centeredView}>
          <View style={styles.modalViewWithoutAlarmPermission}>
            <View style={styles.title}>
              <Text style={styles.text}>Agendar lembrete</Text>
              <TouchableOpacity
                onPress={() => {
                  setVisible(!isVisible);
                }}>
                <Text style={styles.close}>X</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.description}>
              Para agendar um lembrete, por favor, habilite as notificações nas
              configurações do dispositivo.
            </Text>
            <View style={styles.buttonsContainer}>
              <TouchableOpacity
                onPress={() => {
                  setVisible(!isVisible);
                }}
                style={styles.buttonCreateNotification}>
                <Text style={styles.textButtonWithoutAlarmPermission}>
                  Cancelar
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={openSettings}
                style={styles.buttonCreateNotification}>
                <Text style={styles.textButtonWithoutAlarmPermission}>
                  Abrir Configurações
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </Modal>
  );
};

export default ModalRememberNotification;
