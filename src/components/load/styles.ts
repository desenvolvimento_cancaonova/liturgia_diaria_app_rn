import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#194976',
  },
  logoCancao: {
    width: 150,
    height: 150,
    marginBottom: 50,
  },
});

export default styles;
