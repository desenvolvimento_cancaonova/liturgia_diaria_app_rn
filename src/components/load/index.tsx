import React from 'react';
import {ActivityIndicator, Image, Modal, View} from 'react-native';
import styles from './styles';

const Load = () => {
  return (
    <Modal>
      <View style={styles.container}>
        <Image
          style={styles.logoCancao}
          resizeMode="contain"
          source={require('../../assets/images/logoPreload/logo_cancao_nova.png')}
        />
        <ActivityIndicator size="large" color="#fff" />
      </View>
    </Modal>
  );
};

export default Load;
