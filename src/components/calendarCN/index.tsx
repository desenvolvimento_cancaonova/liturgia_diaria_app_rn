import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {Modal, TouchableWithoutFeedback, View} from 'react-native';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import Toast from 'react-native-simple-toast';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';

const CalendarCN = ({isVisible = false, setVisible, onSelectDay}) => {
  const [newHomilia, setNewHomilia] = useState(null);
  const {date, dates, setCalenderDate} = useDate();
  const {theme} = useTheme();

  LocaleConfig.locales['pt-BR'] = {
    monthNames: [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro',
    ],
    monthNamesShort: [
      'Jan.',
      'Fev.',
      'Mar',
      'Abr',
      'Mai',
      'Jun',
      'Jul.',
      'Ago',
      'Set.',
      'Out.',
      'Nov.',
      'Dez.',
    ],
    dayNames: [
      'Domingo',
      'Segunda',
      'Terça',
      'Quarta',
      'Quinta',
      'Sexta',
      'Sábado',
    ],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    today: 'Hoje',
  };

  LocaleConfig.defaultLocale = 'pt-BR';

  const createDate = () => {
    if (dates) {
      let newDateHomilia = {};
      dates.forEach(item => {
        if (moment(item).format('L') !== moment(new Date()).format('L')) {
          newDateHomilia[
            moment(item).format('L').split('/').reverse().join('-')
          ] = {selected: true, selectedColor: '#194976'};
        }
      });

      setNewHomilia(newDateHomilia);
    }
  };

  const toast = () => {
    Toast.show('Não há conteúdo nesta data', 3000, {
      backgroundColor: '#fff',
      textColor: '#194976',
    });
  };

  const checkIfDateIsValid = (dateItem: any) => {
    if (
      newHomilia[dateItem.dateString] === undefined &&
      moment(new Date()).format('L').split('/').reverse().join('-') !==
        dateItem.dateString
    ) {
      toast();
      return;
    }
    setCalenderDate(dateItem.dateString);
  };

  useEffect(() => {
    setVisible(false);
  }, [date]);

  useEffect(() => {
    createDate();
  }, [dates]);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}
      onRequestClose={() => {
        setVisible(false);
      }}>
      <TouchableWithoutFeedback onPress={() => setVisible(false)}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#00000030',
          }}>
          {dates && (
            <Calendar
              onDayPress={day => {
                checkIfDateIsValid(day);
              }}
              markedDates={newHomilia}
              hideExtraDays
              theme={{
                calendarBackground: theme !== 'Claro' ? '#414141' : '#ffffff',
                textSectionTitleColor:
                  theme !== 'Claro' ? '#ffffff' : '#b6c1cd',
                dayTextColor: theme !== 'Claro' ? '#ffffff' : '#2d4150',
                textDisabledColor: theme !== 'Claro' ? '#cecece' : '#d9e1e8',
                dotColor: '#000',
                monthTextColor: theme !== 'Claro' ? '#ffffff' : '#000000',
                textColor: '#7a0909',
              }}
            />
          )}
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default CalendarCN;
