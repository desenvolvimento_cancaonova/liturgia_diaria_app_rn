import moment from 'moment';
import React, {useCallback, useEffect, useState} from 'react';
import {View} from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';

const WatchScreen = ({videoUrl, audioStartEvent, setVideoStartEvent}) => {
  const [playing, setPlaying] = useState(false);

  const onStateChange = useCallback(state => {
    if (state === 'ended') {
      setPlaying(false);
    }
    if (state === 'playing') {
      setPlaying(true);
    }
  }, []);

  useEffect(() => {
    if (audioStartEvent !== null) {
      setPlaying(false);
    }
  }, [audioStartEvent]);

  useEffect(() => {
    if (playing === true) {
      setVideoStartEvent(moment().unix());
    }
  }, [playing]);

  return (
    <View>
      <YoutubePlayer
        width={341}
        height={194}
        play={playing}
        videoId={videoUrl}
        onChangeState={onStateChange}
      />
    </View>
  );
};
export default WatchScreen;
