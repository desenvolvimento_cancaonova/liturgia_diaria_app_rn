import React from 'react';
import {StyleSheet, View} from 'react-native';
import {LiturgiaInterface} from '~/interfaces/liturgia';
import {WebView} from '../webview';

interface Props {
  data: LiturgiaInterface;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 17,
  },
});

const Leitura2Tab = ({data}: Props) => {
  return (
    <View style={styles.container}>
      <WebView content={data.leitura2} />
    </View>
  );
};
export default Leitura2Tab;
