import Clipboard from '@react-native-clipboard/clipboard';
import React, {useEffect} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Toast from 'react-native-simple-toast';
import IconPlay from '~/assets/images/botao-play.png';
import IconStop from '~/assets/images/stop.png';
import {usePlayer} from '~/context/appContext';
import {useTheme} from '~/context/themeContext';
import {LiturgiaInterface} from '~/interfaces/liturgia';
import {OracaoInterface} from '~/interfaces/oracao';
import SantoInterface from '~/interfaces/santo';
import {predefinedEvent} from '~/services/firebase';
import {bodyColorDark} from '~/styles/theme';
import removeHtml from '~/utils/RemoveHtml';
import shareItem from '~/utils/Share';
import PlayerHomilia from '../playerHomilia';
import styles from './styles';

interface ControlsProps {
  url: string;
  handleCopy: () => string;
  title: string;
  zoomIn: () => void;
  zoomOut: () => void;
  tab?: number;
  type?: 'liturgia' | 'santo' | 'oracao' | 'homilia';
  model?: LiturgiaInterface & SantoInterface & OracaoInterface;
  audioUrl: string;
}

const Controls: React.FC<ControlsProps> = ({
  url,
  handleCopy,
  title,
  zoomIn,
  zoomOut,
  tab,
  type,
  model,
  audioUrl,
}) => {
  const {theme} = useTheme();

  const {
    isPlayingText,
    tPlayer: {play, stop},
  } = usePlayer();

  const color = () => {
    if (isPlayingText !== 'stop') {
      return IconStop;
    } else {
      return IconPlay;
    }
  };
  const handleShare = () => {
    shareItem(`${title} - Você vai gostar disso! ${url}`);
  };

  const handleCP = () => {
    const text = removeHtml(handleCopy());
    const textFormatted = `${title} - Você vai gostar disso! ${url} \n\n${text}`;
    Clipboard.setString(textFormatted);
    Toast.show(`Conteúdo de ${title} copiado com sucesso`, 3000, {
      backgroundColor: '#fff',
      textColor: '#194976',
    });
  };

  useEffect(() => {
    stop();
  }, [model]);

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <TouchableOpacity
          onPress={() => {
            zoomOut();
            predefinedEvent('diminuir_texto', {item_name: 'diminuir_texto'});
          }}>
          <Image
            style={styles.icon}
            source={
              theme === 'Claro'
                ? require('../../assets/images/font_zoom_less.png')
                : theme === 'Pouco escuro'
                ? require('../../assets/images/fontZoomMedium/font_zoom_less.png')
                : require('../../assets/images/fontZoomDark/font_zoom_less.png')
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            zoomIn();
            predefinedEvent('aumentar_texto', {item_name: 'aumentar_texto'});
          }}
          style={{backgroundColor: 'transparent'}}>
          <Image
            style={styles.icon}
            source={
              theme === 'Claro'
                ? require('../../assets/images/font_zoom_plus.png')
                : theme === 'Pouco escuro'
                ? require('../../assets/images/fontZoomMedium/font_zoom_plus.png')
                : require('../../assets/images/fontZoomDark/font_zoom_plus.png')
            }
          />
        </TouchableOpacity>
      </View>
      {audioUrl ? (
        <PlayerHomilia
          audioUrl={audioUrl}
          title={null}
          navigation={null}
          videoStartEvent={null}
          setAudioStartEvent={null}
          type={type}
        />
      ) : (
        type !== 'homilia' && (
          <TouchableOpacity
            style={{
              ...styles.playerContainer,
              ...bodyColorDark(theme),
              ...(theme !== 'Claro' && {
                borderColor: '#FFF',
                borderWidth: 2,
              }),
            }}
            onPress={() => {
              play({tab, type, model});
              predefinedEvent('CLICKPLAY', {
                tela: type,
                tipo: 'leitura de tela',
              });
            }}>
            <Image style={styles.play} source={color()} />
            <Text style={styles.playerText}>escutar o texto</Text>
          </TouchableOpacity>
        )
      )}
      <View style={styles.rowRight}>
        <TouchableOpacity
          onPress={() => {
            handleShare();
            predefinedEvent('compartilhar', {item_name: 'compartilhar'});
          }}>
          <Image
            style={{...styles.icon, ...styles.iconRight}}
            source={
              theme === 'Claro'
                ? require('../../assets/images/share.png')
                : require('../../assets/images/shareDark.png')
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            handleCP();
            predefinedEvent('selecionar_tudo', {item_name: 'selecionar_tudo'});
          }}>
          <Image
            style={{...styles.icon, ...styles.iconRight}}
            source={
              theme === 'Claro'
                ? require('../../assets/images/copy.png')
                : require('../../assets/images/copyDark.png')
            }
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Controls;
