import {StyleSheet} from 'react-native';
import fonts from '../../styles/fonts';
import normalize from '../../utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: normalize(12),
  },
  row: {
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  rowRight: {
    flexDirection: 'row',
    width: '20%',
    justifyContent: 'flex-end',
  },
  icon: {
    resizeMode: 'contain',
    width: normalize(32),
    height: normalize(32),
    marginLeft: normalize(4),
    marginRight: normalize(4),
  },
  iconRight: {
    width: normalize(22),
    height: normalize(22),
  },
  playerContainer: {
    borderRadius: normalize(20),
    paddingVertical: normalize(6),
    paddingHorizontal: normalize(12),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#194976',
  },
  playerText: {
    fontSize: normalize(14),
    color: '#FFF',
    fontFamily: fonts.REGULAR,
    marginBottom: normalize(2),
  },
  play: {
    resizeMode: 'contain',
    tintColor: '#fff',
    height: normalize(14),
    width: normalize(20),
    marginRight: normalize(2),
  },
});

export default styles;
