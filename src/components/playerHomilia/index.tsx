import React, {useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import TrackPlayer, {Event} from 'react-native-track-player';
import MusicControl, {Command} from 'react-native-music-control';
import {usePlayer} from '~/context/appContext';
import {useTheme} from '~/context/themeContext';
import normalize from '../../utils/DynamicRatio';
import moment from 'moment';
import styles from './styles';
import {
  activateKeepAwake,
  deactivateKeepAwake,
} from '@sayem314/react-native-keep-awake';
import {predefinedEvent} from '~/services/firebase';

interface PlayerHomiliaProps {
  audioUrl: string;
  title: string;
  navigation?: any;
  videoStartEvent: any;
  setAudioStartEvent: any;
  type: string;
}

const PlayerHomilia = ({
  audioUrl,
  title,
  videoStartEvent,
  setAudioStartEvent,
  type,
}: PlayerHomiliaProps) => {
  const {
    aPlayer: {toggle, rev, prev, dismountPlayer},
    isPlaying,
  } = usePlayer();

  const {theme} = useTheme();

  useEffect(() => {
    if (videoStartEvent !== null) {
      if (isPlaying === 'playing') {
        toggle(audioUrl, title, true);
      }
    }
  }, [videoStartEvent]);

  useFocusEffect(
    React.useCallback(() => {
      const unsubscribe = () => {};
      unsubscribe();

      return () => dismountPlayer();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [audioUrl]),
  );

  useEffect(() => {
    if (isPlaying === 'playing') {
      activateKeepAwake();
    } else {
      deactivateKeepAwake();
    }
  }, [isPlaying]);

  const color = () => {
    if (isPlaying === 'playing') {
      if (theme === 'Totalmente escuro') {
        return require('../../assets/images/darkPlay/pause.png');
      } else {
        return require('../../assets/images/pause.png');
      }
    } else {
      if (theme !== 'Totalmente escuro') {
        return require('../../assets/images/play.png');
      } else {
        return require('../../assets/images/darkPlay/play.png');
      }
    }
  };

  useEffect(() => {
    if (Platform.OS === 'ios') {
      TrackPlayer.addEventListener(Event.RemoteJumpBackward, () => {
        rev();
      });

      TrackPlayer.addEventListener(Event.RemoteJumpForward, () => {
        prev;
      });
    }

    if (Platform.OS === 'android') {
      // on iOS, pause playback during audio interruptions (incoming calls) and resume afterwards.
      // As of {{ INSERT NEXT VERSION HERE}} works for android aswell.
      MusicControl.enableBackgroundMode(true);
      // MusicControl.handleAudioInterruptions(true);

      MusicControl.on(Command.play, () => {
        toggle(audioUrl, title);
      });

      // on iOS this event will also be triggered by audio router change events
      // happening when headphones are unplugged or a bluetooth audio peripheral disconnects from the device
      MusicControl.on(Command.pause, () => {
        toggle(audioUrl, title);
      });

      MusicControl.on(Command.skipForward, () => {
        prev();
      });
      MusicControl.on(Command.seekBackward, () => {
        rev();
      });
    }
  }, [audioUrl, prev, rev, title, toggle]);

  return (
    <View
      style={{
        ...styles.container,
        ...((audioUrl === '' || audioUrl === undefined) && {opacity: 0.5}),
      }}>
      <TouchableOpacity onPress={() => rev()}>
        <Image
          style={styles.iconReview}
          source={
            theme !== 'Totalmente escuro'
              ? require('../../assets/images/preview10.png')
              : require('../../assets/images/darkPlay/preview10.png')
          }
        />
      </TouchableOpacity>
      <TouchableOpacity
        disabled={audioUrl === undefined || audioUrl === ''}
        onPress={() => {
          predefinedEvent('CLICKPLAY', {
            tela: type,
            tipo: 'audio',
          });
          toggle(audioUrl, title);
          if (setAudioStartEvent !== null) {
            setAudioStartEvent(moment().unix());
          }
        }}>
        <Image style={styles.play} source={color()} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => prev()}>
        <Image
          style={styles.iconPreview}
          source={
            theme !== 'Totalmente escuro'
              ? require('../../assets/images/review10.png')
              : require('../../assets/images/darkPlay/review10.png')
          }
        />
      </TouchableOpacity>
      {/* <Slider
        duration={duration}
        value={position}
        seek={seek}
        setSeek={setSeek}
        isSeeking={isSeeking}
        setIsSeeking={setIsSeeking}
        pause={TrackPlayer.pause}
        seekTo={TrackPlayer.seekTo}
        play={TrackPlayer.play}
        disabled
      /> */}
    </View>
  );
};
export default PlayerHomilia;
