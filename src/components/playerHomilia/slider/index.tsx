import Controller from '@react-native-community/slider';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import normalize from '../../../utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {
    width: normalize(208),
    flexDirection: 'row',
    alignItems: 'center',
    height: normalize(32),
    opacity: 0,
  },

  controller: {
    height: normalize(2),
    zIndex: 1,
    flex: 1,
    backgroundColor: '#fff',
  },
});

interface SliderProps {
  duration: number;
  value: number;
  seek: number;
  setSeek: (seek: number) => void;
  isSeeking: boolean;
  setIsSeeking: (isSeeking: boolean) => void;
  pause: () => void;
  play: () => void;
  seekTo: (value: number) => void;
  disabled?: boolean;
}

const Slider = ({
  duration,
  value,
  seek,
  setSeek,
  isSeeking,
  setIsSeeking,
  pause,
  play,
  seekTo,
  disabled,
}: SliderProps) => {
  return (
    <View style={styles.container}>
      <Controller
        style={styles.controller}
        value={isSeeking ? seek : value}
        onValueChange={vl => {
          pause();
          setIsSeeking(true);
          setSeek(vl);
        }}
        onSlidingComplete={vl => {
          seekTo(vl);
          play();
        }}
        minimumValue={0}
        maximumValue={100}
        minimumTrackTintColor="transparent"
        maximumTrackTintColor="#fff"
        thumbTintColor="#005A8F"
        disabled={disabled}
      />
    </View>
  );
};
export default Slider;
