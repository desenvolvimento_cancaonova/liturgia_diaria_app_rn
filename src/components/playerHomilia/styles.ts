import {StyleSheet} from 'react-native';
import normalize from '../../utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '45%',
    paddingHorizontal: normalize(10),
    paddingVertical: normalize(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#E1F3F7',
    borderRadius: normalize(20),
  },
  iconReview: {
    resizeMode: 'contain',
    width: normalize(22),
    height: normalize(22),
  },
  play: {
    resizeMode: 'contain',
    width: normalize(22),
    height: normalize(22),
  },
  iconPreview: {
    resizeMode: 'contain',
    width: normalize(22),
    height: normalize(22),
  },
});
export default styles;
