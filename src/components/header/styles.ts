import { StyleSheet } from 'react-native';
import fonts from '../../styles/fonts';
import normalize from '../../utils/DynamicRatio';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#194976',
    flexDirection: 'row',
    padding: normalize(16),
    justifyContent: 'space-between',
    width: '100%',
  },
  headerLeft: {
    flexDirection: 'row',
  },
  headerText: {
    zIndex: 1,
    color: '#fff',
    fontSize: normalize(20),
    fontFamily: fonts.REGULAR,
    width: normalize(110),
  },
  backIcon: {
    height: normalize(20),
    marginRight: 10,
    marginLeft: 0,
    marginTop: 2,
  },
  headerIcon: {
    height: normalize(24),
    justifyContent: 'flex-end',
    marginRight: 10,
    marginLeft: 10,
  },
  buttonsContainer: {
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: normalize(220),
  },
});

export default styles;
