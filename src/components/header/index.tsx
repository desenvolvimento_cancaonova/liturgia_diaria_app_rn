import Clipboard from '@react-native-clipboard/clipboard';
import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {Image, Linking, Text, TouchableOpacity, View} from 'react-native';
import Toast from 'react-native-simple-toast';
import {usePlayer} from '~/context/appContext';
import {useDate} from '~/context/dateContext';
import {useTheme} from '~/context/themeContext';
import {predefinedEvent} from '~/services/firebase';
import {bodyColorDark} from '~/styles/theme';
import CalendarCN from '../calendarCN';
import ModalRememberNotification from '../modalRememberNotification';
import ModalTheme from '../modalTheme';
import styles from './styles';

const Header = ({title, onSelectDay, hasGoBack}: any) => {
  const [modalCalendarVisible, setModalCalendarVisible] = useState(false);
  const [modalThemeVisible, setModalThemeVisible] = useState(false);
  const [modalCreateNotification, setModalCreateNotification] = useState(false);
  const {theme} = useTheme();
  const {ID} = usePlayer();
  const {page} = useDate();
  const navigation = useNavigation();

  const handleCP = () => {
    Clipboard.setString(ID);

    Toast.show('PlayerID copiado com sucesso', 3000, {
      backgroundColor: '#fff',
      textColor: '#194976',
    });
  };

  return (
    <View style={[styles.header, bodyColorDark(theme)]}>
      <View style={styles.headerLeft}>
        {hasGoBack && (
          <TouchableOpacity
            style={styles.backIcon}
            onPress={() => navigation.goBack()}>
            <Image source={require('../../assets/images/back.png')} />
          </TouchableOpacity>
        )}
        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.headerText}>
          {title}
        </Text>
      </View>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={styles.headerIcon}
          onPress={() => {
            setModalThemeVisible(true);
            predefinedEvent('trocar_tema', {item_name: 'trocar_tema'});
          }}>
          <Image source={require('../../assets/images/ic_moon.png')} />
        </TouchableOpacity>
        {(page === 'Homilia' || page === 'Liturgia' || page === 'Santos') && (
          <TouchableOpacity
            style={styles.headerIcon}
            onPress={() => {
              setModalCalendarVisible(true);
              predefinedEvent('abrir_calendario', {
                item_name: 'abrir_calendario',
              });
            }}>
            <Image source={require('../../assets/images/ic_calendar.png')} />
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={styles.headerIcon}
          onPress={() => {
            setModalCreateNotification(true);
            predefinedEvent('agendar_lembrete', {
              item_name: 'agendar_lembrete',
            });
          }}>
          <Image source={require('../../assets/images/ic_timer.png')} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.headerIcon}
          onPress={() => {
            predefinedEvent('Doacoes');
            Linking.openURL('https://secure.cancaonova.com/da/?app=liturgia');
          }}>
          <Image source={require('../../assets/images/colabore.png')} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.headerIcon}
          onPress={() => {
            predefinedEvent('LojaVirtual');
            Linking.openURL(
              'https://loja.cancaonova.com/?utm_source=Menu+Aplicativos&utm_medium=Liturgia+APP&utm_campaign=Liturgia+APP',
            );
          }}>
          <Image source={require('../../assets/images/store.png')} />
        </TouchableOpacity>
      </View>
      <CalendarCN
        isVisible={modalCalendarVisible}
        setVisible={setModalCalendarVisible}
        onSelectDay={onSelectDay}
      />
      <ModalTheme
        isVisible={modalThemeVisible}
        setVisible={setModalThemeVisible}
      />
      <ModalRememberNotification
        isVisible={modalCreateNotification}
        setVisible={setModalCreateNotification}
      />
    </View>
  );
};

export default Header;
