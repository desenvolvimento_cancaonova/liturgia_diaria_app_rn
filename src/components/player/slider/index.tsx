import Controller from '@react-native-community/slider';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {usePlayer} from '~/context/appContext';
import {useTheme} from '~/context/themeContext';
import globalStyle, {bodyColorLight} from '~/styles/theme';
import normalize from '../../../utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {
    width: normalize(208),
    flexDirection: 'row',
    alignItems: 'center',
    height: normalize(32),
    flex: 1,
  },
  controller: {
    height: normalize(2),
    zIndex: 20,
    flex: 1,
    backgroundColor: '#fff',
  },
});

interface SliderProps {
  disabled?: boolean;
}

const Slider = ({disabled}: SliderProps) => {
  const {theme} = useTheme();
  const {time} = usePlayer();

  return (
    <View style={styles.container}>
      <Controller
        disabled={disabled}
        style={[styles.controller, bodyColorLight(theme)]}
        value={time}
        step={1}
        maximumValue={100}
        // onValueChange={onValueChange}
        minimumTrackTintColor="transparent"
        maximumTrackTintColor="transparent"
        // thumbImage={require('~/images/marker_color.png')}
        thumbTintColor={
          theme !== 'Totalmente escuro'
            ? globalStyle.mediumBody.backgroundColor
            : globalStyle.darkBody.backgroundColor
        }
      />
    </View>
  );
};
export default Slider;
