import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {usePlayer} from '~/context/appContext';
import {useTheme} from '~/context/themeContext';
import {LiturgiaInterface} from '~/interfaces/liturgia';
import {OracaoInterface} from '~/interfaces/oracao';
import SantoInterface from '~/interfaces/santo';
import normalize from '../../utils/DynamicRatio';
import Slider from './slider';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: '91%',
    paddingHorizontal: normalize(19),
    paddingVertical: normalize(17),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E1F3F7',
    elevation: 10,
    shadowColor: '#005A8F',
    shadowOpacity: 0.5,
    shadowOffset: {
      width: -1,
      height: 1.5,
    },
    marginBottom: normalize(32),
  },
  icon: {
    resizeMode: 'contain',
    width: normalize(45),
    height: normalize(45),
  },
  play: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: normalize(20),
  },
});

interface PalyerProps {
  tab?: number;
  type: 'liturgia' | 'santo' | 'oracao';
  model: LiturgiaInterface & SantoInterface & OracaoInterface;
}

const Player = ({tab, type, model}: PalyerProps) => {
  const {
    isPlaying,
    tPlayer: {play},
  } = usePlayer();
  const {theme} = useTheme();

  const color = () => {
    if (isPlaying !== 'stop') {
      if (theme === 'Totalmente escuro') {
        return require('../../assets/images/darkPlay/pause.png');
      } else {
        return require('../../assets/images/pause.png');
      }
    } else {
      if (theme !== 'Totalmente escuro') {
        return require('../../assets/images/play.png');
      } else {
        return require('../../assets/images/darkPlay/play.png');
      }
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.play}>
        <TouchableOpacity
          onPress={() => {
            play({tab, type, model});
          }}>
          <Image style={styles.icon} source={color()} />
        </TouchableOpacity>
      </View>

      {/* <Slider disabled /> */}
    </View>
  );
};
export default Player;
