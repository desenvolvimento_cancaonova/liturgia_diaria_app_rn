import { StyleSheet } from 'react-native';
import fonts from '~/styles/fonts';
import normalize from '~/utils/DynamicRatio';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingTop: 15,
    paddingHorizontal: 10,
    marginBottom: normalize(10),
  },
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  dateNumber: {
    fontSize: normalize(30),
    fontWeight: 'bold',
    marginRight: normalize(10),
    fontFamily: fonts.BOLD,
    color: '#253138',
  },
  monthYear: {
    fontSize: normalize(14),
    textAlign: 'center',
    fontFamily: fonts.REGULAR,
    color: '#253138',
  },
  desc: {
    fontSize: normalize(14),
    textAlign: 'center',
    fontFamily: fonts.BOLD,
  },
  color: {fontFamily: fonts.BOLD},
  title: {
    marginTop: 12,
    fontSize: normalize(18),
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: fonts.BOLD,
    color: '#000',
  },
});

export default styles;