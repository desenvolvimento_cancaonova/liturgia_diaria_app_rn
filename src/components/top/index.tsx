import React from 'react';
import { Text, View } from 'react-native';
import { useTheme } from '~/context/themeContext';
import { Colors } from '~/interfaces/liturgia';
import { bodyColorLight, textColorLight } from '~/styles/theme';
import styles from './style';


type Props = {
  title?: string;
  color?: keyof typeof Colors;
  dia_formatado: string;
  mes: string;
  ano: string;
};

const Top = ({title, color, dia_formatado, mes, ano}: Props) => {
  const {theme} = useTheme();

  const colors = {
    branca: '#000',
    verde: '#2DA5A5',
    roxa: '#323278',
    vermelha: '#D70708',
  };

  const colorsName = {
    branca: 'Branca',
    verde: 'Verde',
    roxa: 'Roxa',
    vermelha: 'Vermelha',
  };

  const getColors = (value: keyof typeof Colors) => {
    return colors[value];
  };

  return (
    <View style={[styles.container, bodyColorLight(theme)]}>
      <View style={styles.dateContainer}>
        <Text style={[styles.dateNumber, textColorLight(theme)]}>
          {dia_formatado.toUpperCase()}
        </Text>
        <View>
          <Text style={[styles.monthYear, textColorLight(theme)]}>
            {mes.toUpperCase()}
          </Text>
          <Text style={[styles.monthYear, textColorLight(theme)]}>
            {ano.toUpperCase()}
          </Text>
        </View>
      </View>
      {color ? (
        <Text
          style={[
            styles.desc,
            {color: getColors(color)},
            textColorLight(theme),
          ]}>
          {'Cor Litúrgica: '}
          <Text style={[styles.color, textColorLight(theme)]}>
            {colorsName[`${color}`]}
          </Text>
        </Text>
      ) : (
        <Text />
      )}
      {title && (
        <Text style={[styles.title, textColorLight(theme)]}>{title}</Text>
      )}
    </View>
  );
};

export default Top;
