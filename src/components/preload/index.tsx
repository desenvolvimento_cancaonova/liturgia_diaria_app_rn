import React from 'react';
import {ActivityIndicator, Image, Modal, Text, View} from 'react-native';
import styles from './styles';

const Preload = (info: any) => {
  if (info !== null) {
    return (
      <View style={styles.container}>
        <View style={{position: 'absolute'}}>
          <Image
            style={styles.logoLiturgia}
            resizeMode="contain"
            source={require('../../assets/images/logoPreload/logo_liturgia.png')}
          />
        </View>

        <View
          style={{
            backgroundColor: '#00000050',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{backgroundColor: '#00000080', width: '80%', height: 150}}>
            <Text style={{color: 'white', margin: 18, fontSize: 24}}>
              Atualizações
            </Text>
            <View
              style={{
                flexDirection: 'row',
                paddingTop: 10,
                paddingLeft: 18,
                alignItems: 'center',
              }}>
              <ActivityIndicator size="large" color="#fff" />
              <Text style={{color: 'white', marginLeft: 24, fontSize: 18}}>
                Atualizando {info.info}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  } else {
    return null;
  }
};

export default Preload;
