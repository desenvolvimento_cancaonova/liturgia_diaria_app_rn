import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#194976',
    width: '100%',
    height: '100%',
  },
  logoLiturgia: {
    width: 150,
    height: 150,
  },
});

export default styles;
