/* eslint-disable @typescript-eslint/no-shadow */

import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {
  forwardRef,
  ReactNode,
  Ref,
  useImperativeHandle,
  useRef,
} from 'react';
import {Linking} from 'react-native';
import {Dimensions, Platform} from 'react-native';
import AutoHeightWebView, {
  AutoHeightWebViewProps,
} from 'react-native-autoheight-webview';
import {useTheme} from '~/context/themeContext';
import {bodyColorLight} from '~/styles/theme';

export interface WB {
  WView: ReactNode;
}
declare module 'react' {
  function forwardRef<T, P = {}>(
    render: (props: P, ref: React.Ref<T>) => React.ReactElement | null,
  ): (props: P & React.RefAttributes<T>) => React.ReactElement | null;
}
interface WebViewInterface extends AutoHeightWebViewProps {
  content: string;
  oracao?: boolean;
  isModal?: boolean;
}

type WebViewRef = Ref<unknown>;

const Web: React.FC<WebViewInterface> = (
  {content, isModal = false},
  ref: WebViewRef,
) => {
  const wbRef: any = useRef();
  const {theme, textZoom, setTextZoom} = useTheme();
  const changeZoom = async (value: number) => {
    await AsyncStorage.setItem('@zoom', JSON.stringify(value));
  };

  const iosTextLimit = 400;
  const androidTextLimit = 225;

  const zoomOut = () => {
    if (textZoom > 100) {
      setTextZoom(textZoom - 25);
      changeZoom(textZoom - 25);
    }

    if (Platform.OS === 'ios' && wbRef.current) {
      wbRef.current.injectJavaScript('zoomOut(); true;');
    }

    if (Platform.OS === 'android' && wbRef.current) {
      wbRef.current.reload();
    }
  };

  const zoomIn = () => {
    if (Platform.OS === 'ios' && textZoom >= iosTextLimit) {
      return;
    } else if (Platform.OS === 'android' && textZoom >= androidTextLimit) {
      return;
    }

    setTextZoom(textZoom + 25);
    changeZoom(textZoom + 25);

    if (Platform.OS === 'ios' && wbRef.current) {
      wbRef.current.injectJavaScript('zoomIn(); true;');
    }

    if (Platform.OS === 'android' && wbRef.current) {
      wbRef.current.reload();
    }
  };

  useImperativeHandle(ref, () => ({
    zoomIn: () => {
      zoomIn();
    },
    zoomOut: () => {
      zoomOut();
    },
  }));

  const handleLinkPress = event => {
    const url = event.url;
    Linking.openURL(url);
    return false;
  };

  const handleNavigationChange = event => {
    const url = event.url;
    if (url && url !== 'about:blank' && !url.startsWith('file://')) {
      Linking.openURL(url);
      wbRef.current.stopLoading();
    }
  };

  return (
    <>
      {textZoom !== 0 && (
        <AutoHeightWebView
          style={[
            bodyColorLight(theme),
            {
              width: Dimensions.get('window').width,
              marginTop: 20,
            },
          ]}
          ref={wbRef}
          originWhitelist={['*']}
          viewportContent={'width=device-width, user-scalable=no'}
          customStyle={`body {background-color: ${
            theme === 'Pouco escuro'
              ? '#194976'
              : theme === 'Totalmente escuro' && '#253138'
          }; color:${theme === 'Claro' ? '#000' : '#fff'};  width: 90%; `}
          source={{
            html: `${content}
                          <script>
                            function zoomIn() {
                              if(fontSize < 60) { fontSize += 1; document.body.style.fontSize = fontSize+"pt";  }
                            }
                            function zoomOut() {
                              if(fontSize > 10) { fontSize -= 1; document.body.style.fontSize = fontSize+"pt";  }
                            }
                            function getDefaultFontSize(e) {
                                e = e || document.body;
                                var t = document.createElement("div");
                                (t.style.cssText = "display:inline-block; padding:10; line-height:1; position:absolute; visibility:hidden; font-size:1em"), t.appendChild(document.createTextNode("M")), e.appendChild(t);
                                var o = t.offsetHeight;
                                return e.removeChild(t), o;
                            }
                            var fontSize = ${textZoom * 0.1};
                            document.body.style.fontSize = "${textZoom * 0.1}pt"
                          </script>
                          `,
          }}
          mediaPlaybackRequiresUserAction={false}
          {...(Platform.OS === 'android'
            ? {onShouldStartLoadWithRequest: handleLinkPress}
            : {onNavigationStateChange: handleNavigationChange})}
          scalesPageToFit={false}
          javaScriptEnabled={true}
          textZoom={textZoom}
          mixedContentMode={'compatibility'}
          onMessage={e => {}}
          scrollEnabled={isModal ? true : false}
          onError={e => console.log({e})}
          removeClippedSubviews={true}
          scrollEnabledWithZoomedin={false}
        />
      )}
    </>
  );
};
export const WebView = forwardRef(Web);
