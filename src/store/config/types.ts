export interface IConfigStore {
  playAudioInBackground: boolean;
  setPlayAudioInBackground: (d: boolean) => void;
  lastUpdateTime: Date | string;
  setlastUpdate: (time: Date | string) => void;
}
