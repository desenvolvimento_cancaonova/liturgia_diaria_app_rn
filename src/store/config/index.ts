import {create} from 'zustand';
import {persist, createJSONStorage} from 'zustand/middleware';
import {IConfigStore} from './types';
import AsyncStorage from '@react-native-async-storage/async-storage';

const useConfigStore = create(
  persist<IConfigStore>(
    (set, _) => ({
      playAudioInBackground: false,
      lastUpdateTime: 'no-data',
      setlastUpdate: value => set(() => ({lastUpdateTime: value})),
      setPlayAudioInBackground: value =>
        set(() => ({playAudioInBackground: value})),
    }),
    {
      name: 'liturgia-config-storage',
      storage: createJSONStorage(() => AsyncStorage),
    },
  ),
);

export default useConfigStore;
