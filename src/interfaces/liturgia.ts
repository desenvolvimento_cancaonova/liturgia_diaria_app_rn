export type DataProps = {
  dia: string;
  mes: string;
  ano: string;
};

export enum Colors {
  'branca',
  'verde',
  'roxa',
  'vermelha',
}
export interface LiturgiaInterface {
  id: number;
  titulo: string;
  leitura1: string;
  leitura2: string;
  cabecalho: string;
  data: DataProps;
  dia_formatado: string;
  mes: string;
  ano: string;
  salmo: string;
  evangelho: string;
  dia: Date;
  url: string;
  texto?: string;
  modified: Date;
  lastdate: Date;
  cor: keyof typeof Colors;
  audio_leitura1: string;
  audio_leitura2: string;
  audio_salmo: string;
  audio_evangelho: string;
}

export interface LeiturasCabecalhoMenuProps {
  leitura1: string;
  leitura2: string;
  salmo: string;
  evangelho: string;
}
