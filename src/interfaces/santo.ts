export type DataProps = {
  dia: string;
  mes: string;
};
export default interface SantoInterface {
  id: number;
  titulo: string;
  texto?: string;
  data: DataProps;
  dia_formatado: string;
  mes: string;
  modified: Date;
  dia: Date;
}
