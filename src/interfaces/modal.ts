import {NotificationsInterface} from './notifications';

export interface modal {
  isVisible: boolean;
  setVisible: (prev: boolean) => boolean;
  notificationData?: NotificationsInterface;
}
