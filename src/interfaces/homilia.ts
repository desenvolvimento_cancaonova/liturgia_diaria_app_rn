export type DataProps = {
  dia: string;
  mes: string;
  ano: string;
};

export default interface HomiliaInterface {
  lastdate: Date;
  error: boolean;
  id: number;
  modified: Date;
  titulo: string;
  audio: string;
  url: string;
  video: string;
  dia: Date;
  cabecalho: string;
  data: DataProps;
  dia_formatado: string;
  mes: string;
  ano: string;
  texto?: string;
}
