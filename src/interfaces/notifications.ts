export interface NotificationsInterface {
  id: number;
  title: string;
  content: string;
  date: string;
  expired_in: string;
  isRead?: boolean;
}
