export interface OracaoCategoriaInterface {
  id: number;
  nome: string;
}

export interface OracaoInterface {
  id: number;
  nome: string;
  categoria: OracaoCategoriaInterface;
  dia: string;
  titulo: string;
  imagem: string;
  url: string;
  texto: string;
  modified: Date;
}
