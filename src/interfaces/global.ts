import {NavigationStackProp} from 'react-navigation-stack';

type Navigation = {
  navigate: (name: string, params?: any) => void;
  goBack: () => void;
  dispatch: () => void;
};

export interface NavigationProps {
  navigation: Navigation;
}

export interface navigationProps {
  navigation: NavigationStackProp<any, any>;
}
