import {StyleSheet} from 'react-native';

export const globalStyle = StyleSheet.create({
  lightBody: {
    backgroundColor: '#ffffff',
  },
  mediumBody: {
    backgroundColor: '#194976',
  },
  darkBody: {
    backgroundColor: '#253138',
  },
  lightText: {
    color: '#FFFFFF',
  },
  darkText: {
    color: '#253138',
  },
  lightBlue: {
    backgroundColor: '#E1F3F7',
  },
  lightTextGray: {
    color: '#e1e1e1',
  },
});

export const textColorLight = (theme: String) => {
  return theme !== 'Claro' && globalStyle.lightText;
};

export const textColorLightGray = (theme: String) => {
  return theme !== 'Claro' && globalStyle.lightTextGray;
};

export const textColorDark = (theme: String) => {
  return theme !== 'Claro' && globalStyle.darkText;
};

export const bodyColorMedium = (theme: String) => {
  return theme === 'Pouco escuro'
    ? globalStyle.mediumBody
    : theme === 'Totalmente escuro' && globalStyle.darkBody;
};

export const bodyLightBlue = (theme: String) => {
  return theme !== 'Claro' && globalStyle.lightBlue;
};

export const bodyLightDark = (theme: String) => {
  return theme !== 'Claro' ? globalStyle.lightBody : globalStyle.mediumBody;
};

export const darkText = (theme: String) => {
  return theme !== 'Claro' ? globalStyle.darkText : globalStyle.lightText;
};

export const bodyColorLight = (theme: String) => {
  return theme === 'Claro'
    ? globalStyle.lightBody
    : theme === 'Pouco escuro'
    ? globalStyle.mediumBody
    : globalStyle.darkBody;
};

export const bodyColorDark = (theme: String) => {
  return theme === 'Totalmente escuro' && globalStyle.darkBody;
};

export default globalStyle;
