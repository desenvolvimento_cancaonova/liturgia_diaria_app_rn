const fonts = {
  THIN: 'Lato-Thin', //100
  LIGHT: 'Lato-Light', //300
  REGULAR: 'Lato-Regular', //400
  BOLD: 'Lato-Bold', //700
  BOLDER: 'Lato-Black', //900
  ITALIC: 'Lato-Italic',
};

export default fonts;
