import {IEventParams, IScreenParams} from 'src/libs/firebase/modules/analytics';
import * as repository from '../repository';
const AddAnalyticsEventService = async (name: string, params?: IEventParams) =>
  repository.analytics.addEvent(name, params);
const AddAnalyticsLogScreenService = async (params: IScreenParams) =>
  repository.analytics.logScreen(params);
export {AddAnalyticsEventService, AddAnalyticsLogScreenService};
