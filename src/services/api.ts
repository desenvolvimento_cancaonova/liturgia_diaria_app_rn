import axios, {AxiosInstance} from 'axios';

// import * as AxiosLogger from 'axios-logger';

const createApi = (base_url: string): AxiosInstance => {
  const _api = axios.create({
    baseURL: base_url,
    headers: {
      'Content-Type': 'application/json',
      'Accept-Language': 'pt-br',
      Accept: 'application/json',
    },
  });

  // if (APP.ENV === 'dev') {
  //_api.interceptors.request.use(AxiosLogger.requestLogger);
  // }
  return _api;
};

export default createApi;
