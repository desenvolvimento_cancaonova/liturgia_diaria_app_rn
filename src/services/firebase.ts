import {
  AddAnalyticsEventService,
  AddAnalyticsLogScreenService,
} from './AnalyticsServices';

export const predefinedEvent = async (event: string, params?: any) => {
  AddAnalyticsEventService(event, params);
};

export const screenView = async (event: any) => {
  AddAnalyticsLogScreenService({screen_class: event});
};
