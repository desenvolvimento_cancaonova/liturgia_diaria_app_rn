export const removeTagBody = (item: string) => {
  const regex = /<body.*?>([\s\S]*)<\/body>/gm;
  const body = regex.exec(`${item}`);
  const text = body[0];

  return text;
};

export const removeTagFirst = (item: string) => {
  return item.split("<body onLoad='checkImageOn();'>")[1];
};

export const removeTagEnd = (item: string) => {
  return item.split('</body>')[0];
};

export const removeTag = (item: string) => {
  const itemFormatted = item.replace(/<[^>]*>?/gm, '').replace('&nbsp;', '');

  return itemFormatted;
};
