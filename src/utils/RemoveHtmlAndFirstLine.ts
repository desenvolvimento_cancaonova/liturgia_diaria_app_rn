import removeHtml from './RemoveHtml';

const removeHtmlAndFirstLine = (text: string) => {
  let result = removeHtml(text);
  let resultArray = result.split('\n');
  resultArray[0] = '';

  return resultArray.join('\n');
};

export default removeHtmlAndFirstLine;
