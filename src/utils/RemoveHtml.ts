import sanitize from 'sanitize-html';
const removeHtml = (text: string) => {
  const regex = /<[^>]*>?/gm;
  const regexKeys = /{[^}]*}?/gm;
  const expStr = /h[0-9]/gm;
  const regexWords = /.day,|.month,|.year|.m-y|.day/gi;

  const result = text
    .replace(regex, '')
    .replace(regexKeys, '')
    .replace(expStr, '')
    .replace(regexWords, '')
    .trim();

  return sanitize(result);
};

export default removeHtml;
