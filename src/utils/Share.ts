import Share from 'react-native-share';

const shareItem = async (url: string) => {
  const title = 'Canção Nova';

  try {
    Share.open({
      title,
      subject: title,
      message: url,
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  } catch (error) {
    console.log('Error =>', error);
  }
};

export default shareItem;
