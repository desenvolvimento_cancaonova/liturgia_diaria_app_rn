import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';

import {APlayer, AudioPlayer} from '~/libs/AudioPlayer';
import {TextPlayer, TPlayer} from '~/libs/textPlayer';

export enum PlayerState {
  'playing',
  'stop',
  'pause',
}
export interface PlayerContextType {
  tPlayer: TPlayer;
  aPlayer: APlayer;
  isPlaying: keyof typeof PlayerState;
  isPlayingText: keyof typeof PlayerState;
  setObserver: (object: object) => void;
  time: number;
  observer: object;
  ID: string;
  setID: (ID: string) => void;
}

type AuthproviderProps = {children: ReactNode};

export const PlayerContext = createContext({} as PlayerContextType);

function AuthProvider({children}: AuthproviderProps) {
  const [isPlaying, setIsPlaying] = useState<keyof typeof PlayerState>('stop');
  const [isPlayingText, setIsPlayingText] =
    useState<keyof typeof PlayerState>('stop');
  const {play, stop, setup} = TextPlayer({isPlayingText, setIsPlayingText});
  const {
    position,
    prev,
    setIsSeeking,
    setSeek,
    rev,
    toggle,
    isSeeking,
    seek,
    duration,
    dismountPlayer,
  } = AudioPlayer({isPlaying, setIsPlaying});

  const [observer, setObserver] = useState(null);
  const [time, settime] = useState(1);
  const [ID, setID] = useState(null);

  useEffect(() => {
    stop();
    settime(1);
  }, [observer]);

  useEffect(() => {
    if (isPlaying === 'playing') {
      const interval = setInterval(() => {
        settime(time + 1);
      }, 3000);
      return () => clearInterval(interval);
    }
  }, [isPlaying, time]);

  return (
    <PlayerContext.Provider
      value={{
        time,
        ID,
        setID,
        observer,
        setObserver,
        isPlaying,
        isPlayingText,
        tPlayer: {play, stop, setup},
        aPlayer: {
          position,
          prev,
          rev,
          toggle,
          isSeeking,
          seek,
          duration,
          dismountPlayer,
          setIsSeeking,
          setSeek,
        },
      }}>
      {children}
    </PlayerContext.Provider>
  );
}

function usePlayer() {
  const context = useContext(PlayerContext);

  return context;
}

export {AuthProvider, usePlayer};
