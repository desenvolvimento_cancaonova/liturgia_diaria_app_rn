import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import moment from 'moment';
import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import Toast from 'react-native-simple-toast';
import HomiliaInterface from '~/interfaces/homilia';
import {LiturgiaInterface} from '~/interfaces/liturgia';
import {
  default as Homilia,
  default as HomiliaModel,
} from '../repository/database/model/homilia';
import {
  default as Liturgia,
  default as LiturgiaModel,
} from '../repository/database/model/liturgia';
import SantoModel from '../repository/database/model/santo';
('../repository/database/model/santo.ts');
import crashlytics from '@react-native-firebase/crashlytics';
import Notifications from '~/repository/database/model/notifications';
import {NotificationsInterface} from '~/interfaces/notifications';

type AuthproviderProps = {children: ReactNode};

interface dateContextType {
  retry: () => void;
  setDate: (item: Date) => void;
  liturgiaModel: LiturgiaInterface;
  homiliaModel: HomiliaInterface;
  santoModel: object;
  date: Date;
  getBydayLiturgia: (date: string, dates: any) => void;
  getBydayHomilia: (date: string) => void;
  dates: Array<String>;
  setDates: (item: Array<String>) => void;
  setCalenderDate: (item: string) => void;
  setPage: (item: string) => void;
  page: string;
  isEnabled: boolean;
  pushInAppEnabled: boolean;
  setIsEnabled: (item: boolean) => void;
  setPushInAppEnabled: (item: boolean) => void;
  receivePushNotification: (item: boolean) => void;
  receivePushInAppNotification: (item: boolean) => void;
  getPushInAppNotificationStatus: (item: boolean) => void;
  getAllNotifications: NotificationsInterface[];
  markAsRead: (id: number) => void;
}

export const DateContext = createContext({} as dateContextType);

function DateProvider({children}: AuthproviderProps) {
  const [date, setDate] = useState(new Date());
  const [liturgiaModel, setliturgiaModel] = useState<LiturgiaInterface>(null);
  const [homiliaModel, sethomiliaModel] = useState<HomiliaInterface>(null);
  const [santoModel, setSantoModel] = useState(null);
  const [dates, setDates] = useState(null);
  const [calenderDate, setCalenderDate] = useState(null);
  const [page, setPage] = useState(null);
  const [isEnabled, setIsEnabled] = useState(true);
  const [pushInAppEnabled, setPushInAppEnabled] = useState();

  const getLiturgia = useCallback(async () => {
    const model = new LiturgiaModel().getByDia(date);
    setliturgiaModel(model);
  }, [date]);

  const getHomilia = useCallback(() => {
    const model = new HomiliaModel().getByDia(date);

    sethomiliaModel(model);
  }, [date]);

  const getSantos = useCallback(() => {
    setSantoModel(new SantoModel().getByDia(date));
  }, [date]);

  const retry = () => {
    try {
      setliturgiaModel(new LiturgiaModel().getByDia(date));
      sethomiliaModel(new HomiliaModel().getByDia(date));
      setSantoModel(new SantoModel().getByDia(date));
    } catch (error: any) {
      crashlytics().recordError(
        new Error(`retry Liturgia error: ${error.message}`),
      );
    }
  };

  const getDataLiturgia = async (dt: string) => {
    dt = dt.split('-').reverse().join('/');
    try {
      const data = await axios.get(
        `https://liturgia.cancaonova.com/pb/json-liturgia/?date=${dt}`,
      );
      return data?.data ? data.data.liturgias : null;
    } catch (error) {
      console.log('Error to GET /json-liturgia/?v=2');
    }
    return null;
  };

  const getDataHomilia = useCallback(async (dt: string) => {
    dt = dt.split('-').reverse().join('/');
    try {
      const url = `https://homilia.cancaonova.com/pb/json-homilia/?v=2&date=${dt}`;
      console.log('Calling: GET', url);
      const data = await axios.get(url);
      return data?.data ? data.data.homilias : null;
    } catch (error) {
      console.log('Error to GET /json-homilia/?v=2');
    }
    return null;
  }, []);

  const getBydayLiturgia = useCallback(
    async (dt: string) => {
      try {
        if (dates.some(item => +item === +moment(dt))) {
          setDate(calenderDate);
          getLiturgia();
          return;
        }
        let newDate = moment(date);
        if (!dates.some(item => +item === +newDate)) {
          const mockDate = {
            data: {
              dia: dt.split('-')[2],
              mes: dt.split('-')[1],
              ano: dt.split('-')[0],
            },
          };
          const liturgia = await getDataLiturgia(dt);
          liturgia[0].leitura1 =
            '<strong>Primeira Leitura' +
            liturgia[0].leitura1.split('<strong>Primeira Leitura')[1];
          liturgia[0].salmo =
            '<strong>Responsório' +
            liturgia[0].salmo.split('<strong>Responsório')[1];
          liturgia[0].evangelho =
            '<strong>Evangelho' +
            liturgia[0].evangelho.split('<strong>Evangelho')[1];
          let newDateForm = Object.assign(mockDate, liturgia[0]);
          const model = new Liturgia();
          [newDateForm].forEach((it: LiturgiaInterface) => {
            it.lastdate = moment(it.lastdate).toDate();
            it.dia = moment(
              String(it.dia).split('/').reverse().join('-'),
            ).toDate();
            it.modified = moment(it.modified).toDate();
            it.dia_formatado = it.data.dia;
            it.ano = it.data.ano;
            it.mes = it.data.mes;
            model.save(it);
          });
        }
        setDate(calenderDate);
      } catch {
        Toast.show('A data selecionada ainda não contem o conteúdo', 3000, {
          backgroundColor: '#fff',
          textColor: '#194976',
        });
      }
    },
    [calenderDate, date, dates, getLiturgia],
  );

  const getBydayHomilia = useCallback(
    async (dt: string) => {
      try {
        if (dates.some(item => +item === +moment(dt))) {
          setDate(calenderDate);
          getHomilia();
          return;
        }
        const homilia = await getDataHomilia(dt);
        const modelHomilia = new Homilia();
        homilia.forEach((it: HomiliaInterface) => {
          it.lastdate = moment(it.lastdate).toDate();
          it.dia = moment(
            String(it.dia).split('/').reverse().join('-'),
          ).toDate();
          it.modified = moment(it.modified).toDate();
          it.dia_formatado = it.data.dia;
          it.ano = it.data.ano;
          it.mes = it.data.mes;
          modelHomilia.save(it);
        });
        setDate(calenderDate);
      } catch (e) {
        Toast.show('A data selecionada ainda não contem o conteúdo', {
          position: Toast.position.CENTER,
          containerStyle: {
            backgroundColor: '#DDDDDD',
            borderRadius: 15,
          },
          textStyle: {
            color: '#194976',
          },
        });
      }
    },
    [calenderDate, dates, getHomilia],
  );

  const receivePushNotification = async (value: boolean) => {
    setIsEnabled(value);
  };

  const receivePushInAppNotification = async (value: boolean) => {
    await AsyncStorage.setItem('@pushInAppNotification', JSON.stringify(value));
    setPushInAppEnabled(value);
  };

  const getPushInAppNotificationStatus = async () => {
    const value = await AsyncStorage.getItem('@pushInAppNotification');
    if (value !== null) {
      setPushInAppEnabled(Boolean(JSON.parse(value)));
    } else {
      setPushInAppEnabled(true);
    }
  };

  const getAllNotifications = async (): Promise<NotificationsInterface[]> => {
    return new Notifications().getAllNotifications();
  };

  const markAsRead = async (id: number) => {
    new Notifications().markAsRead(id);
  };

  useEffect(() => {
    if (calenderDate) {
      if (page === 'Liturgia') {
        getBydayLiturgia(calenderDate);
        getLiturgia();
      }
      if (page === 'Homilia') {
        getBydayHomilia(calenderDate);
        getHomilia();
      }
      if (page === 'Santos') {
        setDate(calenderDate);
        getSantos();
      }
    }
  }, [
    calenderDate,
    date,
    getBydayHomilia,
    getBydayLiturgia,
    getHomilia,
    getLiturgia,
    getSantos,
    page,
  ]);

  useEffect(() => {
    getLiturgia();
    getHomilia();
    getSantos();
    getPushInAppNotificationStatus();
  }, []);

  return (
    <DateContext.Provider
      value={{
        date,
        setDate,
        liturgiaModel,
        homiliaModel,
        santoModel,
        getBydayLiturgia,
        getBydayHomilia,
        dates,
        setDates,
        setCalenderDate,
        setPage,
        page,
        isEnabled,
        setIsEnabled,
        receivePushNotification,
        getAllNotifications,
        markAsRead,
        retry,
        pushInAppEnabled,
        setPushInAppEnabled,
        receivePushInAppNotification,
        getPushInAppNotificationStatus,
      }}>
      {children}
    </DateContext.Provider>
  );
}

function useDate() {
  const context = useContext(DateContext);

  return context;
}

export {DateProvider, useDate};
