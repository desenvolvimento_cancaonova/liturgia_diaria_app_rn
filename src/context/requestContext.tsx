import React, {createContext, ReactNode, useCallback, useContext} from 'react';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HomiliaApi from '~/repository/api/homilia';
import LiturgiaApi from '~/repository/api/liturgia';
import OracaoApi from '~/repository/api/oracao';
import SantoApi from '~/repository/api/santo';
import NotificationsApi from '~/repository/api/notifications';
import Homilia from '~/repository/database/model/homilia';
import Liturgia from '~/repository/database/model/liturgia';
import Oracao from '~/repository/database/model/oracao';
import Notifications from '~/repository/database/model/notifications';
import Santo from '~/repository/database/model/santo';
import {postDataHomilia} from '~/repository/api/homilia/post';
import {postDataLiturgia} from '~/repository/api/liturgia/post';

type RequestProviderProps = {children: ReactNode};

interface RequestContextType {
  getHomilia: () => Promise<void>;
  getLiturgia: () => Promise<void>;
  getSanto: () => Promise<void>;
  getOracao: () => Promise<void>;
  getNotifications: () => Promise<void>;
}

export const RequestContext = createContext({} as RequestContextType);

const RequestProvider: React.FC<RequestProviderProps> = ({children}) => {
  const changeDate = async () => {
    await AsyncStorage.setItem('@date', moment().format('L'));
  };

  const changeWeek = async () => {
    await AsyncStorage.setItem('@week', moment().format('L'));
  };

  const getLiturgia = async () => {
    changeDate();
    const dbLiturgia = new Liturgia().getAll();
    if (!dbLiturgia) {
      let NewArray: {id: number; modified: string}[] = [];
      dbLiturgia.forEach(item =>
        NewArray.push({id: item.id, modified: item.modified}),
      );
      const update = await postDataLiturgia(NewArray);
      const model = new Liturgia();
      for (let i = 0; i < update.length; i++) {
        const it = update[i];
        it.lastdate = moment(it.lastdate).toDate();
        it.dia = moment(it.dia).toDate();
        it.modified = moment(it.modified).toDate();
        it.dia_formatado = it.data.dia;
        it.ano = it.data.ano;
        it.mes = it.data.mes;
        model.save(it);
      }
    }
    const liturgia = await LiturgiaApi.get();
    if (liturgia) {
      const model = new Liturgia();
      for (let i = 0; i < liturgia.length; i++) {
        const it = liturgia[i];
        it.lastdate = moment(it.lastdate).toDate();
        it.dia = moment(it.dia).toDate();
        it.modified = moment(it.modified).toDate();
        it.dia_formatado = it.data.dia;
        it.ano = it.data.ano;
        it.mes = it.data.mes;
        model.save(it);
      }
    }
  };

  const getHomilia = async () => {
    changeWeek();
    const dbHomilia = new Homilia().getAll();
    if (!dbHomilia) {
      let NewArray: {id: number; modified: string}[] = [];
      dbHomilia.forEach(item =>
        NewArray.push({id: item.id, modified: moment(item.modified)}),
      );
      const update = await postDataHomilia(NewArray);
      const model = new Homilia();
      for (let i = 0; i < update.length; i++) {
        const it = update[i];
        it.lastdate = moment(it.lastdate).toDate();
        it.dia = moment(it.dia).toDate();
        it.modified = moment(it.modified).toDate();
        it.dia_formatado = it.data.dia;
        it.ano = it.data.ano;
        it.mes = it.data.mes;
        model.save(it);
      }
    }
    const homilia = await HomiliaApi.get();
    if (homilia) {
      const model = new Homilia();
      for (let i = 0; i < homilia.length; i++) {
        const it = homilia[i];
        it.lastdate = moment(it.lastdate).toDate();
        it.dia = moment(it.dia).toDate();
        it.modified = moment(it.modified).toDate();
        it.dia_formatado = it.data.dia;
        it.ano = it.data.ano;
        it.mes = it.data.mes;
        model.save(it);
      }
    }
  };

  const getSanto = async () => {
    const santo = await SantoApi.get();
    if (santo) {
      const model = new Santo();
      for (let i = 0; i < santo.length; i++) {
        const it = santo[i];
        it.modified = moment(it.modified).toDate();
        it.dia_formatado = it.data.dia;
        it.mes = it.data.mes;
        model.save(it);
      }
    }
  };

  const getOracao = async () => {
    const oracao = await OracaoApi.get();
    if (oracao) {
      const model = new Oracao();
      for (let i = 0; i < oracao.length; i++) {
        model.save(oracao[i]);
      }
    }
  };

  const getNotifications = async () => {
    const notifications = await NotificationsApi.get();
    if (notifications) {
      const model = new Notifications();
      for (let i = 0; i < notifications.length; i++) {
        model.save(notifications[i]);
      }
    }
  };

  return (
    <RequestContext.Provider
      value={{
        getHomilia,
        getLiturgia,
        getOracao,
        getSanto,
        getNotifications,
      }}
      children={children}
    />
  );
};

const useRequest = () => {
  const context = useContext(RequestContext);
  return context;
};

export {RequestProvider, useRequest};
