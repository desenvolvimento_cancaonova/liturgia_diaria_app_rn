import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';

export interface ThemeContextType {
  setTheme: (item: string) => void;
  theme: string;
  changeTheme: (item: string) => void;
  textZoom: number;
  setTextZoom: (item: number) => void;
}

type AuthproviderProps = {children: ReactNode};

export const ThemeContext = createContext({} as ThemeContextType);

function ThemeProvider({children}: AuthproviderProps) {
  const [theme, setTheme] = useState('Claro');
  const [textZoom, setTextZoom] = useState(100);

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@theme');
      if (value !== null) {
        setTheme(value);
      }
    } catch {
      setTheme('Claro');
    }
  };

  const changeTheme = async (value: string) => {
    await AsyncStorage.setItem('@theme', value);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <ThemeContext.Provider
      value={{
        setTheme,
        theme,
        changeTheme,
        setTextZoom,
        textZoom,
      }}>
      {children}
    </ThemeContext.Provider>
  );
}

function useTheme() {
  const context = useContext(ThemeContext);
  return context;
}

export {ThemeProvider, useTheme};
