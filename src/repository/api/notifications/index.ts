import axios from 'axios';
import {NotificationsInterface} from '~/interfaces/notifications';

export default {
  async get(): Promise<NotificationsInterface[]> {
    try {
      const data = await axios.get(
        'https://liturgia.cancaonova.com/pb/wp-json/v1/inbox',
      );
      return data?.data ? data.data : null;
    } catch (error) {
      console.log(
        'Error to GET https://liturgia.cancaonova.com/pb/wp-json/v1/inbox',
      );
    }
    return null;
  },
};
