import SantoInterface from '~/interfaces/santo';
import createApi from '../../../services/api';

const api = createApi('https://santo.cancaonova.com');

export default {
  async get(): Promise<SantoInterface[]> {
    try {
      const data = await api.get('/santo-json/?v=2');
      return data?.data ? data.data.santos : null;
    } catch (error) {
      console.log('Erro santo');
    }
    return null;
  },
};
