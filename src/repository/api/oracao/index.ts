import {OracaoInterface} from '~/interfaces/oracao';
import createApi from '../../../services/api';
import moment from 'moment';

const api = createApi('https://liturgia.cancaonova.com/pb');

export default {
  async get(): Promise<OracaoInterface[]> {
    try {
      const data = await api.get(
        `/json-oracoes?${moment().format('YYYYMMDDHHmm')}`,
      );
      return data?.data ? data.data.oracoes : null;
    } catch (error) {
      console.log('Error to GET /json-oracoes');
    }
    return null;
  },
};
