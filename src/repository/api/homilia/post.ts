import axios from 'axios';

export const postDataHomilia = async newArray => {
  try {
    const url = 'https://homilia.cancaonova.com/pb/json-homilia/?v=2';
    const data = await axios.post(url, {
      homilias: newArray,
    });
    console.log('Called: POST', url, {data});
    return data?.data ? data.data.homilias : null;
  } catch (error) {
    console.log('Error to POST /json-homilia/?v=2 (postDataHomilia)');
  }
  return null;
};
