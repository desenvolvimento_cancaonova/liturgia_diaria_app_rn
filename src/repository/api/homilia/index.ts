import axios from 'axios';
import moment from 'moment';
import HomiliaInterface from '~/interfaces/homilia';

export default {
  async get(): Promise<HomiliaInterface[]> {
    try {
      const url = `https://homilia.cancaonova.com/pb/json-homilia/?v=2&read_file=true&${moment().unix()}`;

      console.log('Calling: GET', url);
      const data = await axios.get(url);

      return data?.data ? data.data.homilias : null;
    } catch (error) {
      console.log(
        'Error to GET /json-homilia/?v=2&read_file=true',
        JSON.stringify(error),
      );
    }
    return null;
  },
};
