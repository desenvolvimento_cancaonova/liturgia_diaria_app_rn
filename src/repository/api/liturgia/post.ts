import axios from 'axios';
import moment from 'moment';

export const postDataLiturgia = async newArray => {
  try {
    const data = await axios.post(
      'https://liturgia.cancaonova.com/pb/json-liturgia/?v=2',
      {
        liturgias: newArray,
      },
    );
    return data?.data ? data.data.liturgias : null;
  } catch (error) {
    console.log('Error to POST /json-liturgia/?v=2');
  }
  return null;
};
