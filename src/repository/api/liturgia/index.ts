import axios from 'axios';
import {LiturgiaInterface} from '../../../interfaces/liturgia';
// import createApi from '../../../services/api';

// const api = createApi('https://liturgia.cancaonova.com/pb');

export default {
  async get(): Promise<LiturgiaInterface[]> {
    try {
      const data = await axios.get(
        'https://liturgia.cancaonova.com/pb/json-liturgia/?v=2',
      );
      return data?.data ? data.data.liturgias : null;
    } catch (error) {
      console.log('Error to GET son-liturgia/?v=2');
    }
    return null;
  },
};
