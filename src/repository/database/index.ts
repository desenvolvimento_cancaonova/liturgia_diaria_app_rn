import Realm from 'realm';

import {
  LiturgiaSchema,
  HomiliaSchema,
  SantoSchema,
  OracaoSchema,
  OracaoCategoriaSchema,
  NotificationsSchema,
} from './schemas';

const schemas = [
  LiturgiaSchema,
  HomiliaSchema,
  SantoSchema,
  OracaoSchema,
  OracaoCategoriaSchema,
  NotificationsSchema,
];

let realm: any;

const migrations = (oldRealm, newRealm) => {
  if (oldRealm.schemaVersion < 6) {
    newRealm.objects('Liturgia').forEach(liturgia => {
      liturgia.audio_leitura1 = '';
      liturgia.audio_leitura2 = '';
      liturgia.audio_salmo = '';
      liturgia.audio_evangelho = '';
    });
  }

  if (oldRealm.schemaVersion < 7) {
    const oldNotifications = oldRealm.objects('Notification');
    const newNotifications = newRealm.objects('Notification');

    for (let i = 0; i < oldNotifications.length; i++) {
      newNotifications[i].isRead = false;
    }
  }
};

const getRealm = () => {
  if (!realm) {
    realm = new Realm({
      schema: schemas,
      schemaVersion: 7,
      migration: migrations,
    });
  }

  return realm;
};

export {getRealm};
