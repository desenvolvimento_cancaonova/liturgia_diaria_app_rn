import {ObjectSchema} from 'realm';

export const LiturgiaSchema: ObjectSchema = {
  name: 'Liturgia',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: ''},
    titulo: {type: 'string', default: ''},
    cabecalho: {type: 'string', default: ''},
    dia_formatado: {type: 'string', default: ''},
    mes: {type: 'string', default: ''},
    ano: {type: 'string', default: ''},
    leitura1: {type: 'string', default: ''},
    leitura2: {type: 'string', default: ''},
    salmo: {type: 'string', default: ''},
    evangelho: {type: 'string', default: ''},
    rodape: {type: 'string', default: ''},
    lastdate: {type: 'date', default: ''},
    dia: {type: 'date', default: 0.0},
    url: {type: 'string', default: 0.0},
    modified: {type: 'date', default: ''},
    cor: {type: 'string', default: ''},
    audio_leitura1: {type: 'string', default: ''},
    audio_leitura2: {type: 'string', default: ''},
    audio_salmo: {type: 'string', default: ''},
    audio_evangelho: {type: 'string', default: ''},
  },
};

export const HomiliaSchema: ObjectSchema = {
  name: 'Homilia',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: ''},
    titulo: {type: 'string', default: ''},
    texto: {type: 'string', default: ''},
    cabecalho: {type: 'string', default: ''},
    dia_formatado: {type: 'string', default: ''},
    mes: {type: 'string', default: ''},
    ano: {type: 'string', default: ''},
    audio: {type: 'string', default: ''},
    video: {type: 'string', default: ''},
    dia: {type: 'date', default: 0.0},
    url: {type: 'string', default: 0.0},
    modified: {type: 'date', default: ''},
    lastdate: {type: 'date', default: ''},
    error: {type: 'bool', default: ''},
  },
};

export const SantoSchema: ObjectSchema = {
  name: 'Santo',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: ''},
    titulo: {type: 'string', default: ''},
    texto: {type: 'string', default: ''},
    cabecalho: {type: 'string', default: ''},
    dia_formatado: {type: 'string', default: ''},
    mes: {type: 'string', default: ''},
    dia: {type: 'string', default: 0.0},
    url: {type: 'string', default: 0.0},
    modified: {type: 'date', default: ''},
  },
};

export const OracaoCategoriaSchema: ObjectSchema = {
  name: 'OracaoCategoria',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: ''},
    nome: {type: 'string', default: ''},
  },
};

export const OracaoSchema: ObjectSchema = {
  name: 'Oracao',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: ''},
    titulo: {type: 'string', default: ''},
    texto: {type: 'string', default: ''},
    url: {type: 'string', default: ''},
    imagem: {type: 'string', default: ''},
    categoria: 'OracaoCategoria',
  },
};

export const NotificationsSchema: ObjectSchema = {
  name: 'Notifications',
  primaryKey: 'id',
  properties: {
    id: {type: 'int', default: ''},
    title: {type: 'string', default: ''},
    content: {type: 'string', default: ''},
    date: {type: 'string', default: ''},
    expired_in: {type: 'string', default: ''},
    isRead: {type: 'bool', default: false},
  },
};
