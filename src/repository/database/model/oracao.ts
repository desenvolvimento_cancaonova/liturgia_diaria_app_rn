import {OracaoInterface} from '~/interfaces/oracao';
import Base from './base';
import {removeTagBody} from '~/utils/newHtml';

class Oracao extends Base {
  constructor() {
    super('Oracao');
  }

  textoToCopy(model: OracaoInterface): string {
    const textWithoutSpaces = model.texto
      .replace(/&nbsp;/g, '')
      .replace(/\s+/g, ' ');
    const text = `${model.titulo}
    \n${removeTagBody(textWithoutSpaces)}`;
    return text;
  }
}

export default Oracao;
