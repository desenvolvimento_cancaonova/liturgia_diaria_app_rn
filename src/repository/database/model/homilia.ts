import moment from 'moment';
import HomiliaInterface from '../../../interfaces/homilia';
import Base from './base';

class HomiliaModel extends Base {
  constructor() {
    super('Homilia');
  }

  textoToCopy(model: HomiliaInterface): string {
    const text = `${model.dia_formatado}/${model.mes.toUpperCase()}/${model.ano}
    \n${model.texto}`;
    return text;
  }

  formatedText(text: string) {
    const txt = text.split('Amém!');
    if (txt.length > 1) {
      return (
        txt[0].replace(/<\/audio*?([\s\S]*)\/audio>/, '') +
        'Amém!' +
        '<p>&nbsp;</p>' +
        txt[1].replace(/<\/p*?([\s\S]*)\/p>/, '') +
        '<p>&nbsp;</p>'
      );
    }
    return 'text';
  }
  getByDia(value: Date): HomiliaInterface {
    return super.getByDia(value);
  }

  async clearDays(daysLeft: number = 10): Promise<void> {
    const rows: HomiliaInterface[] = this.getAll();

    const cutDate = moment()
      .subtract(daysLeft, 'days')
      .hour(0)
      .minute(0)
      .second(0);

    const mustBeDeleted = [];

    let i;
    for (i = 0; i < rows.length; i++) {
      const {id, dia} = rows[i];
      if (moment(dia) < cutDate) {
        mustBeDeleted.push(id);
      }
    }

    for (i = 0; i < mustBeDeleted.length; i++) {
      this.delete(mustBeDeleted[i]);
    }
  }
}

export default HomiliaModel;
