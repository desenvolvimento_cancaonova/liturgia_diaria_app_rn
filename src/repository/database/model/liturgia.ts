import moment from 'moment';
import {LiturgiaInterface} from '../../../interfaces/liturgia';
import Base from './base';
import he from 'he';

class Liturgia extends Base {
  constructor() {
    super('Liturgia');
  }

  textoToCopy(model: LiturgiaInterface, value: number): string {
    const leituras: any = {
      0: model.leitura1,
      1: model.leitura2,
      2: model.salmo,
      3: model.evangelho,
    };
    const colorsName = {
      branca: 'Branca',
      verde: 'Verde',
      roxa: 'Roxa',
      vermelha: 'Vermelha',
    };
    const text = `${model.dia_formatado}/${model.mes.toUpperCase()}/${
      model.ano
    }\nCor Litúrgica: ${colorsName[`${model.cor}`]}\n${
      model.titulo
    }\n${he.decode(leituras[value])}`;
    return text;
  }

  getByDia(value: Date): LiturgiaInterface {
    return super.getByDia(value);
  }

  async clearDays(daysLeft: number = 10): Promise<void> {
    const rows: LiturgiaInterface[] = this.getAll();
    const cutDate = moment()
      .subtract(daysLeft, 'days')
      .hour(0)
      .minute(0)
      .second(0);

    const mustBeDeleted = [];

    for (let i = 0; i < rows.length; i++) {
      const {id, dia} = rows[i];
      if (moment(dia) < cutDate) {
        mustBeDeleted.push(id);
      }
    }

    mustBeDeleted.map(id => this.delete(id));
  }
}

export default Liturgia;
