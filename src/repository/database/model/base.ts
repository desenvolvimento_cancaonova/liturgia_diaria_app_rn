import moment from 'moment';
import uuid from 'react-native-uuid';
import crashlytics from '@react-native-firebase/crashlytics';
import {getRealm} from '../index';

class Base {
  protected _schemeName: string;
  protected _realm: Realm;

  constructor(schemaName: string) {
    this._schemeName = schemaName;
    this._realm = getRealm();
  }

  all(filter = {}, options = {}) {
    const data = this._realm.objects(this._schemeName);
    let results = data;

    const filters = [];
    if (filter !== null) {
      Object.keys(filter).forEach(field => {
        if (typeof filter[field] === 'string') {
          filters.push(`${field} == '${filter[field]}'`);
        } else if (
          typeof filter[field] === 'object' &&
          filter[field] !== null
        ) {
          // console.log('field: ', field, filter[field]);
          if (filter[field] !== null) {
            const arrayFilter = [];

            filter[field].forEach(item => {
              arrayFilter.push(`${field} == ${item}`);
            });

            filters.push(`(${arrayFilter.join(' OR ')})`);
          }
        } else if (filter[field] === null) {
          filters.push(`${field}.@count == 0`);
        } else {
          filters.push(`${field} == ${filter[field]}`);
        }
      });

      try {
        if (filters.length > 1) {
          results = data.filtered(filters.join(' AND '));
        } else if (filters.length === 1) {
          results = data.filtered(filters[0]);
        }
      } catch (e) {
        console.log(
          'MODEL::BASE::ALL::FILTERED',
          `Schema::${this._schemeName}`,
          {e, filters},
        );
      }
    }

    if (options?.sorted) {
      const sorted =
        typeof options?.sorted[1] === 'boolean'
          ? [options?.sorted]
          : options?.sorted;
      results = results.sorted(sorted);
    }

    return [...results];
  }

  getAll() {
    const data = this._realm.objects(this._schemeName);
    return data;
  }

  getByDia(value: Date) {
    value = moment(value).hour(0).minute(0).second(0).millisecond(0).toDate();
    const data = this._realm
      .objects(this._schemeName)
      .filtered('dia = $0', value);

    if (data.length) {
      return data[0];
    }
    return false;
  }

  get(id: string) {
    const data = this._realm.objects(this._schemeName).filtered(`id = '${id}'`);

    if (data.length > 0) {
      return data[0];
    }

    return false;
  }

  create(data, update = true) {
    try {
      this._realm.write(() => {
        if (!Object.keys(data).includes('id') || !data?.id) {
          data.id = this.creataPrimaryKeyId();
        }

        this._realm.create(this._schemeName, data, update);
      });
      return data.id;
    } catch (error: any) {
      console.log('CREATE ERROR: ', this._schemeName, error);
      crashlytics().recordError(new Error(`deleteAll::models error: ${error}`));
    }
    return false;
  }

  edit(id, data) {
    try {
      const row = this.get(id);
      this._realm.write(() => {
        Object.keys(data).forEach(key => {
          if (key !== 'id' && row[key] !== undefined) {
            row[key] = data[key];
          }
        });
      });
    } catch (error: any) {
      console.log('MODEL::BASE::EDIT::ERROR::', `Schema::${this._schemeName}`, {
        error,
        id,
        data,
      });
      crashlytics().recordError(new Error(`deleteAll::models error: ${error}`));
      return false;
    }

    return true;
  }

  delete(id: string) {
    try {
      const row = this.get(id);
      this._realm.write(() => {
        this._realm.delete(row);
      });
    } catch (error: any) {
      crashlytics().recordError(new Error(`delete::models error: ${error}`));
      throw new Error(error);
    }
  }

  deleteAll() {
    try {
      this._realm.write(() => {
        const objects = this._realm.objects(this._schemeName);
        this._realm.delete(objects);
      });
    } catch (error: any) {
      crashlytics().recordError(new Error(`deleteAll::models error: ${error}`));
      throw new Error(error);
    }
  }

  creataPrimaryKeyId() {
    return uuid.v4();
  }

  save(data, update = false) {
    if (!data?.id) {
      return {...this.create(data), novoRegistro: true};
    }

    const row = this.get(data.id);
    if (!row) {
      return {...this.create(data), novoRegistro: true};
    }
    this.edit(data.id, data);
    return {...data, novoRegistro: false};
  }
}

export default Base;
