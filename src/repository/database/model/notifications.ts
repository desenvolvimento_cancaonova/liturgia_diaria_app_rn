import {NotificationsInterface} from '~/interfaces/notifications';
import Base from './base';
import {EventEmitter} from 'events';
import moment from 'moment';

const NotificationsEmitter = new EventEmitter();

const todayDate = moment().format('DD/MM/YYYY');
const todayDateMoment = moment(todayDate, 'DD/MM/YYYY');

class Notifications extends Base {
  constructor() {
    super('Notifications');
  }

  existsUnreadMessage(): Boolean {
    const data = this._realm
      .objects(this._schemeName)
      .filtered('isRead = false');

    const unreadMessages = data.filter(notification => {
      const expiredInMoment = moment(notification.expired_in, 'DD/MM/YYYY');
      return expiredInMoment.isSameOrAfter(todayDateMoment);
    });

    return unreadMessages.length > 0 ? true : false;
  }

  getAllNotifications(): NotificationsInterface[] {
    const data = this._realm.objects(this._schemeName);

    const notifications = data.filter(notification => {
      const expiredInMoment = moment(notification.expired_in, 'DD/MM/YYYY');
      return expiredInMoment.isSameOrAfter(todayDateMoment);
    }) as unknown as NotificationsInterface[];

    return notifications;
  }

  markAsRead(id: number) {
    this._realm.write(() => {
      const notification = this._realm
        .objects(this._schemeName)
        .filtered(`id = ${id}`) as unknown as NotificationsInterface[];
      notification[0].isRead = true;
    });
    NotificationsEmitter.emit('notificationRead', id);
  }

  static on(event, callback) {
    NotificationsEmitter.on(event, callback);
  }

  static off(event, callback) {
    NotificationsEmitter.off(event, callback);
  }

  static emit(event, data) {
    NotificationsEmitter.emit(event, data);
  }
}

export default Notifications;
