import moment from 'moment';
import SantoInterface from '~/interfaces/santo';
import { removeTag } from '~/utils/newHtml';
import Base from './base';

class Santo extends Base {
  constructor() {
    super('Santo');
  }

  textoToCopy(model: SantoInterface): string {
    const anoAtual = new Date().getFullYear();
    const text = `${
      model.dia_formatado
    }/${model.mes.toUpperCase()}/${anoAtual.toString()}\n${model.titulo}\n${
      removeTag(model.texto)
    }`;
    return text;
  }

  getByDia(value: Date): SantoInterface {
    value = moment(value).format('DD/MM');

    const data = this._realm
      .objects(this._schemeName)
      .filtered('dia = $0', value);

    if (data.length > 0) {
      return data[0];
    }
    return false;
  }
}

export default Santo;
