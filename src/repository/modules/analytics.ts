import {IEventParams, IScreenParams} from 'src/libs/firebase/modules/analytics';
import * as firebase from '../../libs/firebase';
const addEvent = async (name: string, params?: IEventParams) =>
  firebase.analytics.addEvent(name, params);
const logScreen = async (params: IScreenParams) =>
  firebase.analytics.logScreen(params);
export {addEvent, logScreen};
