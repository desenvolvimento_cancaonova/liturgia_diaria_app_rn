import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';
import {Image, View} from 'react-native';
import OneSignal from 'react-native-onesignal';
import Header from './components/header';
import {usePlayer} from './context/appContext';
import {useDate} from './context/dateContext';
import {useTheme} from './context/themeContext';
import Homilia from './screens/homilia';
import {MyTabs} from './screens/liturgia/tabs';
import Oracao from './screens/oracao';
import Santos from './screens/santos';
import Sobre from './screens/sobre';
import Update from './screens/update';
import {predefinedEvent} from './services/firebase';
import globalStyle from './styles/theme';
import normalize from './utils/DynamicRatio';
import Notifications from './repository/database/model/notifications';

const Stack = createNativeStackNavigator();

function Routes() {
  const {setID} = usePlayer();
  const {pushInAppEnabled} = useDate();

  function init() {
    OneSignal.setAppId('3b6ccb71-139e-4c63-a10b-7aab1fa571c0');
    OneSignal.pauseInAppMessages(!pushInAppEnabled);
    OneSignal.promptForPushNotificationsWithUserResponse();
    OneSignal.setNotificationWillShowInForegroundHandler(
      notificationReceivedEvent => {
        // console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
        let notification = notificationReceivedEvent.getNotification();
        // console.log("notification: ", notification);
        const data = notification.additionalData;
        console.log('additionalData: ', data);
        // Complete with null means don't show a notification.
        notificationReceivedEvent.complete(notification);
      },
    );
    //Method for handling notifications opened
    OneSignal.setNotificationOpenedHandler(notification => {
      console.log('OneSignal: notification opened:', notification);
    });
    OneSignal.getDeviceState().then(state => {
      setID(state.userId);
    });
  }

  React.useEffect(() => {
    if (pushInAppEnabled === true || pushInAppEnabled === false) {
      init();
    }
  }, [pushInAppEnabled]);

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          statusBarTranslucent: true,
        }}>
        <Stack.Screen name="Update" component={Update} />
        <Stack.Screen name="Main" component={BottomMenu} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const Tab = createBottomTabNavigator();

const BottomMenu = routes => {
  const [unreadMessages, setUnreadMessages] = React.useState<boolean>(false);

  React.useEffect(() => {
    const checkUnreadMessages = async () => {
      const result = await new Notifications().existsUnreadMessage();
      setUnreadMessages(!!result);
    };

    checkUnreadMessages();

    const handleNotificationRead = () => {
      checkUnreadMessages();
    };

    Notifications.on('notificationRead', handleNotificationRead);

    return () => {
      Notifications.off('notificationRead', handleNotificationRead);
    };
  }, []);

  let {theme} = useTheme();
  const {setObserver} = usePlayer();

  React.useEffect(() => {
    setObserver(routes);
  }, [routes, setObserver]);

  return (
    <Tab.Navigator
      initialRouteName="Liturgia"
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarLabelStyle: {
          paddingHorizontal: 10,
        },
        tabBarStyle: {
          height: normalize(45),
          backgroundColor:
            theme === 'Claro'
              ? globalStyle.mediumBody.backgroundColor
              : globalStyle.lightBlue.backgroundColor,
        },
        tabBarInactiveTintColor: theme === 'Claro' ? '#CCCCCC' : '#19497670',
        tabBarActiveTintColor: theme === 'Claro' ? '#fff' : '#194976',
      })}>
      <Tab.Screen
        name="BottomMenu2"
        component={BottomLiturgia}
        listeners={({navigation, route}) => ({
          tabPress: e =>
            predefinedEvent('acessar_liturgia', {
              item_name: 'acessar_liturgia',
            }),
        })}
        options={{
          unmountOnBlur: true,
          title: 'Liturgia',
          tabBarIcon: ({size, focused}) => {
            return (
              <Image
                style={{
                  marginTop: normalize(5),
                  width: normalize(20),
                  height: normalize(20),
                  opacity: focused ? 1 : 0.7,
                }}
                source={
                  theme === 'Claro'
                    ? require('./assets/images/menu/ic_liturgia.png')
                    : require('./assets/images/menu/iconsBar/ic_liturgia.png')
                }
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Homilia"
        component={Homilia}
        listeners={({navigation, route}) => ({
          tabPress: e =>
            predefinedEvent('acessar_homilia', {
              item_name: 'acessar_homilia',
            }),
        })}
        options={{
          unmountOnBlur: true,
          title: 'Homilia',
          tabBarIcon: ({size, focused}) => {
            return (
              <Image
                style={{
                  marginTop: normalize(5),
                  width: normalize(20),
                  height: normalize(20),
                  opacity: focused ? 1 : 0.7,
                }}
                source={
                  theme === 'Claro'
                    ? require('./assets/images/menu/ic_homilia.png')
                    : require('./assets/images/menu/iconsBar/ic_homilia.png')
                }
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Santos"
        component={Santos}
        listeners={({navigation, route}) => ({
          tabPress: e =>
            predefinedEvent('acessar_santo', {
              item_name: 'santo',
            }),
        })}
        options={{
          unmountOnBlur: true,
          title: 'Santos',
          tabBarIcon: ({size, focused}) => {
            return (
              <Image
                style={{
                  marginTop: normalize(5),
                  width: normalize(23),
                  height: normalize(23),
                  opacity: focused ? 1 : 0.7,
                }}
                source={
                  theme === 'Claro'
                    ? require('./assets/images/menu/ic_santo.png')
                    : require('./assets/images/menu/iconsBar/ic_santo.png')
                }
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Orações"
        component={Oracao}
        listeners={({navigation, route}) => ({
          tabPress: e =>
            predefinedEvent('acessar_oracoes', {
              item_name: 'acessar_oracoes',
            }),
        })}
        options={{
          unmountOnBlur: true,
          title: 'Orações',
          tabBarIcon: ({size, focused}) => {
            return (
              <Image
                style={{
                  marginTop: normalize(5),
                  width: normalize(20),
                  height: normalize(20),
                  opacity: focused ? 1 : 0.7,
                }}
                source={
                  theme === 'Claro'
                    ? require('./assets/images/menu/ic_oracao.png')
                    : require('./assets/images/menu/iconsBar/ic_oracao.png')
                }
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Sobre"
        component={Sobre}
        listeners={({navigation, route}) => ({
          tabPress: e =>
            predefinedEvent('acessar_sobre', {
              item_name: 'acessar_sobre',
            }),
        })}
        options={{
          title: 'Sobre',
          tabBarIcon: ({size, focused}) => {
            return (
              <>
                <Image
                  style={{
                    marginTop: normalize(5),
                    width: normalize(20),
                    height: normalize(20),
                    opacity: focused ? 1 : 0.7,
                  }}
                  source={
                    theme === 'Claro'
                      ? require('./assets/images/menu/ic_sobre.png')
                      : require('./assets/images/menu/iconsBar/ic_sobre.png')
                  }
                />
                {unreadMessages && (
                  <View
                    style={{
                      position: 'absolute',
                      right: 15,
                      top: 10,
                      width: 10,
                      height: 10,
                      borderRadius: 5,
                      backgroundColor: 'red',
                    }}
                  />
                )}
              </>
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

const Top = createMaterialTopTabNavigator();

const BottomLiturgia = () => {
  return (
    <Top.Navigator
      initialRouteName="Liturgia"
      tabBar={() => (
        <Header
          title={'Liturgia'}
          onSelectDay={undefined}
          navigation={undefined}
          hasGoBack={undefined}
        />
      )}>
      <Top.Screen name="Liturgia" component={MyTabs} />
    </Top.Navigator>
  );
};

export default Routes;
