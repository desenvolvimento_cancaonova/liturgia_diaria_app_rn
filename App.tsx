import React, {useEffect, useState} from 'react';
import {LogBox, Platform, SafeAreaView, StatusBar} from 'react-native';
import {AuthProvider} from '~/context/appContext';
import {DateProvider} from '~/context/dateContext';
import {RequestProvider} from '~/context/requestContext';
import {ThemeProvider} from './src/context/themeContext';

LogBox.ignoreLogs([
  '`new NativeEventEmitter()` was called with a non-null argument without the required `addListener` method.',
]); // Ignore log notification by message
LogBox.ignoreAllLogs();

import Routes from './src/routes';
import {useKeepAwake} from '@sayem314/react-native-keep-awake';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Liturgia from '~/repository/database/model/liturgia';
import HomiliaModel from '~/repository/database/model/homilia';
import Santo from '~/repository/database/model/santo';
import Oracao from '~/repository/database/model/oracao';

const App = () => {
  const [checked, setChecked] = useState(false);
  useKeepAwake();

  const checkIfMustUpdateDB = async () => {
    const now = new Date().getTime();
    const limit = 7200000; // 2h => 7200000; 2min => 120000
    const limitSantos = 43200000; // 12h => 43200000; 2min => 120000
    const lastUpdateTime = await AsyncStorage.getItem('lastUpdateTime');
    const lastUpdateSantosTime = await AsyncStorage.getItem(
      'lastUpdateSantosTime',
    );
    const diff = now - Number(lastUpdateTime);
    const diffSantos = now - Number(lastUpdateSantosTime);

    if (!lastUpdateTime || diff >= limit) {
      const liturgia = new Liturgia();
      const homilia = new HomiliaModel();
      const oracoes = new Oracao();
      liturgia.deleteAll();
      homilia.deleteAll();
      oracoes.deleteAll();
    }

    if (!lastUpdateSantosTime || diffSantos >= limitSantos) {
      const santos = new Santo();

      santos.deleteAll();
    }

    setChecked(true);
  };

  useEffect(() => {
    checkIfMustUpdateDB();
  }, []);

  if (!checked) {
    return null;
  }

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: '#194976',
        marginTop: Platform.OS === 'android' ? 19 : 0,
      }}>
      <StatusBar
        translucent={true}
        backgroundColor="#194976"
        barStyle="light-content"
      />
      <RequestProvider>
        <DateProvider>
          <ThemeProvider>
            <AuthProvider>
              <Routes />
            </AuthProvider>
          </ThemeProvider>
        </DateProvider>
      </RequestProvider>
    </SafeAreaView>
  );
};

export default App;
